package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="video")
public class Video {
	
    @Id
    @Column(name="vid")
    @GeneratedValue(strategy = GenerationType.AUTO)
	int vid;
    @Column(name="vname")
	String vname;
    @Column(name="chapterid")
	int chapterid;
    @Column(name="serialno")
	int serialno;
    @Column(name="duratoin")
	String duratoin;
    @Column(name="description")
	String description;
    @Column(name="updatedatetime")
	String updatedatetime;
    @Column(name="freeornot")
	String freeornot;
    
    
	public int getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public int getChapterid() {
		return chapterid;
	}
	public void setChapterid(int chapterid) {
		this.chapterid = chapterid;
	}
	public int getSerialno() {
		return serialno;
	}
	public void setSerialno(int serialno) {
		this.serialno = serialno;
	}
	public String getDuratoin() {
		return duratoin;
	}
	public void setDuratoin(String duratoin) {
		this.duratoin = duratoin;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUpdatedatetime() {
		return updatedatetime;
	}
	public void setUpdatedatetime(String updatedatetime) {
		this.updatedatetime = updatedatetime;
	}
	public String getFreeornot() {
		return freeornot;
	}
	public void setFreeornot(String freeornot) {
		this.freeornot = freeornot;
	}
    
    
    
    

}
