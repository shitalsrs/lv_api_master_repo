package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.StudentCoursePurchased;

public interface StudentCoursePurchasedRepo extends JpaRepository<StudentCoursePurchased,Integer> {

}


