package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.Courses;

public interface CoursesRepo extends JpaRepository<Courses, Integer> {

}
