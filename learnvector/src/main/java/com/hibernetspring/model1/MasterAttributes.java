package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="master_attributes")
public class MasterAttributes {
/*
 
  `attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mcq_batch_size` int(11) DEFAULT NULL,
  `max_concurrent_sessions_allowed_registered` int(11) DEFAULT NULL,
  `max_concurrent_sessions_allowed_paid_registered` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`attribute_id`)
 */
	
	@Id
	@Column(name="attribute_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int attribute_id;
	@Column(name="mcq_batch_size")
	private int mcq_batch_size;
	@Column(name="max_concurrent_sessions_allowed_registered")
	private int max_concurrent_sessions_allowed_registered;
	@Column(name="max_concurrent_sessions_allowed_paid_registered")
	private int max_concurrent_sessions_allowed_paid_registered;
	
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;

	public int getAttribute_id() {
		return attribute_id;
	}

	public void setAttribute_id(int attribute_id) {
		this.attribute_id = attribute_id;
	}

	public int getMcq_batch_size() {
		return mcq_batch_size;
	}

	public void setMcq_batch_size(int mcq_batch_size) {
		this.mcq_batch_size = mcq_batch_size;
	}

	public int getMax_concurrent_sessions_allowed_registered() {
		return max_concurrent_sessions_allowed_registered;
	}

	public void setMax_concurrent_sessions_allowed_registered(int max_concurrent_sessions_allowed_registered) {
		this.max_concurrent_sessions_allowed_registered = max_concurrent_sessions_allowed_registered;
	}

	public int getMax_concurrent_sessions_allowed_paid_registered() {
		return max_concurrent_sessions_allowed_paid_registered;
	}

	public void setMax_concurrent_sessions_allowed_paid_registered(int max_concurrent_sessions_allowed_paid_registered) {
		this.max_concurrent_sessions_allowed_paid_registered = max_concurrent_sessions_allowed_paid_registered;
	}

	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}

	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
	
}
