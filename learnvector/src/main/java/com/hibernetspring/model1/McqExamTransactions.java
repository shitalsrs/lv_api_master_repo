package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;

@Entity
@Table(name ="mcqexam_transactions")
public class McqExamTransactions {
/*
   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `registration_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `mcq_id` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `answered` tinyint(1) DEFAULT NULL,
  `completed_on` datetime DEFAULT NULL,
  `expired` tinyint(1) DEFAULT NULL,
  `answer_option_selected` tinyint(4) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `exam_type` enum('mock','previous_year_papers') DEFAULT NULL,
  `exam_master_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
  
 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="registration_id")
	private int registration_id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="exam_id")
	private int exam_id;
	@Column(name="mcq_id")
	private int mcq_id;
	@Column(name="created_on")
	private int created_on;
	@Column(name="answered")
	private TinyIntTypeDescriptor answered;
	  @Column(name="completed_on")
	private DateTime completed_on;
		@Column(name="answered")
		private TinyIntTypeDescriptor expired;
		@Column(name="deleted")
		private TinyIntTypeDescriptor answer_option_selected;
	  @Column(name="course_id")
		private int course_id;
	  
	  
		@Enumerated(EnumType.STRING)
		@Column(length = 40)
		private examType exam_type;

		public enum examType {
			mock,previous_year_papers
		}
	  
	  @Column(name="exam_master_id")
	  private int exam_master_id;
		@Column(name="deleted")
		private TinyIntTypeDescriptor deleted;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getRegistration_id() {
			return registration_id;
		}
		public void setRegistration_id(int registration_id) {
			this.registration_id = registration_id;
		}
		public int getStudent_id() {
			return student_id;
		}
		public void setStudent_id(int student_id) {
			this.student_id = student_id;
		}
		public int getExam_id() {
			return exam_id;
		}
		public void setExam_id(int exam_id) {
			this.exam_id = exam_id;
		}
		public int getMcq_id() {
			return mcq_id;
		}
		public void setMcq_id(int mcq_id) {
			this.mcq_id = mcq_id;
		}
		public int getCreated_on() {
			return created_on;
		}
		public void setCreated_on(int created_on) {
			this.created_on = created_on;
		}
		public TinyIntTypeDescriptor getAnswered() {
			return answered;
		}
		public void setAnswered(TinyIntTypeDescriptor answered) {
			this.answered = answered;
		}
		public DateTime getCompleted_on() {
			return completed_on;
		}
		public void setCompleted_on(DateTime completed_on) {
			this.completed_on = completed_on;
		}
		public TinyIntTypeDescriptor getExpired() {
			return expired;
		}
		public void setExpired(TinyIntTypeDescriptor expired) {
			this.expired = expired;
		}
		public TinyIntTypeDescriptor getAnswer_option_selected() {
			return answer_option_selected;
		}
		public void setAnswer_option_selected(TinyIntTypeDescriptor answer_option_selected) {
			this.answer_option_selected = answer_option_selected;
		}
		public int getCourse_id() {
			return course_id;
		}
		public void setCourse_id(int course_id) {
			this.course_id = course_id;
		}
		public examType getExam_type() {
			return exam_type;
		}
		public void setExam_type(examType exam_type) {
			this.exam_type = exam_type;
		}
		public int getExam_master_id() {
			return exam_master_id;
		}
		public void setExam_master_id(int exam_master_id) {
			this.exam_master_id = exam_master_id;
		}
		public TinyIntTypeDescriptor getDeleted() {
			return deleted;
		}
		public void setDeleted(TinyIntTypeDescriptor deleted) {
			this.deleted = deleted;
		}
		
		
		
}
