package com.hibernetspring.testapi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.dao.StockDAO;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model.Stock;



@Path("stock")
public class StockResource {

	@POST
	@Path("/addstock")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createStock(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Stock stock = gson.fromJson(request,Stock.class);
			//System.out.println("gson data"+vehical.getV_carNo());
			
			StockDAO stockDAO=new StockDAO();
			stockDAO.saveStock(stock);		
			response.setSuccess(true);
			response.setMessage("stock  was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create stock, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/list/{dates}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStockDetail(@PathParam("dates") String dates){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			StockDAO stockDAO=new StockDAO();
			response.setSuccess(true);
			response.setMessage("Stock Trip List");
			response.setData(stockDAO.getStockList(dates));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get stock, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
    	
    	//return Response.ok().status(200).entity("I am good").build();
		
	}
	
	
	@GET
	@Path("/getstockofday/{dates}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStockDay(@PathParam("dates") String dates){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			StockDAO stockDAO=new StockDAO();
			response.setSuccess(true);
			response.setMessage("Stock Trip List");
			//response.setData(stockDAO.getStockListdaysount(dates));
			response.setData(stockDAO.getStockListday(dates));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get stock, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
    	
    	//return Response.ok().status(200).entity("I am good").build();
		
	}
	
	@GET
	@Path("/getstockofmonth/{month}/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStockMonth(@PathParam("month") String month,@PathParam("year") String year){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			StockDAO stockDAO=new StockDAO();
			response.setSuccess(true);
			response.setMessage("Stock Trip List of Month");
			
			response.setData(stockDAO.getStockListMonth(month,year));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get stock, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
    	
    	//return Response.ok().status(200).entity("I am good").build();
		
	}
			
	@GET
	@Path("/getlast/{dates}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLastRecord(@PathParam("dates") String dates){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			StockDAO stockdao=new StockDAO();
					
			response.setSuccess(true);
			response.setMessage("vehical Trip List");
			
			response.setData(stockdao.getStock(dates));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  vehical trip, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
    	
    	//return Response.ok().status(200).entity("I am good").build();
		
	}
}
