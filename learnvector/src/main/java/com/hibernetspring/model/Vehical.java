package com.hibernetspring.model;

import javax.persistence.*;


//http://timezra.blogspot.com/2009/05/mapping-hibernate-entities-to-views.html
@Entity
@Table(name="vehical")
public class Vehical{

	@Id
	@Column(name="v_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int v_id;
	
	@Column(name="v_number")
    private int v_number;
	@Column(name="v_emptWeight")
    private int v_emptWeight;
	
	@Column(name="v_entrydate")
    private String v_entrydate;
	
	@Column(name="v_datetime")
    private String v_datetime;
	
	
	
	
	public int getV_id() {
		return v_id;
	}
	public void setV_id(int v_id) {
		this.v_id = v_id;
	}
	public int getV_number() {
		return v_number;
	}
	public void setV_number(int v_number) {
		this.v_number = v_number;
	}
	public int getV_emptWeight() {
		return v_emptWeight;
	}
	public void setV_emptWeight(int v_emptWeight) {
		this.v_emptWeight = v_emptWeight;
	}
	public String getV_entrydate() {
		return v_entrydate;
	}
	public void setV_entrydate(String v_entrydate) {
		this.v_entrydate = v_entrydate;
	}
	public String getV_datetime() {
		return v_datetime;
	}
	public void setV_datetime(String v_datetime) {
		this.v_datetime = v_datetime;
	}
    
}
