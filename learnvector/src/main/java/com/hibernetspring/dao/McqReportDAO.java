package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


import com.hibernetspring.model.Mcqreport;


public class McqReportDAO {
	
	
	public McqReportDAO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean saveMcqReport(Mcqreport mcq)
	{
		Session sessionMcq=HibernateUtil.getSessionFactory().openSession();
		boolean success = false;
		try{
			
			
			sessionMcq.beginTransaction();
			sessionMcq.save(mcq);
			sessionMcq.getTransaction().commit();			
	        success = true;
	         sessionMcq.close();
			
		
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("mcq not created"+e.getMessage());
			
		      success = false;
        }

        return success;
		
	}
	
	
	public List<Object> getAllreportmcq(String userid,String subid,String chapterid)
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionMcq=HibernateUtil.getSessionFactory().openSession();
		
		try{
	
			
			String hql = "FROM Mcqreport m where m.chapterid=:chapterid and m.userid=:userid";
			
			 int getid=1;
				Query query = sessionMcq.createQuery(hql);
				query.setParameter("userid",userid);
				//query.setParameter("subjectid",subid);
				query.setParameter("chapterid",chapterid);
				
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Object> veh=(ArrayList<Object>)results;	
			
		    
		    return veh;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionMcq.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
}
