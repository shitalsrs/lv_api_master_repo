package com.hibernetspring.dao;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.SharedSessionContract;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;

public class HibernateUtil {
	   private static SessionFactory sessionFactory;
	   private HibernateUtil() {
	    }
	   
	   
	   public static synchronized SessionFactory getSessionFactory() {

	        if (sessionFactory == null) {
	        	sessionFactory = new Configuration().configure("hibernate.cfg.xml").
	                    buildSessionFactory();
	        	
	        	
	        }
	        return sessionFactory;
	    }   
	   
      /* public static SessionFactory getSessionFactory() {
             return sessionFactory;
       }*/
}
