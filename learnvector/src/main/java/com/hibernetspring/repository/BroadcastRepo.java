package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.Broadcasting;

public interface BroadcastRepo extends JpaRepository<Broadcasting, Integer> {

}
