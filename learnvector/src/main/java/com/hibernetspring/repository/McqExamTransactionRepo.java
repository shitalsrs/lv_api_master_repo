package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hibernetspring.model1.McqExamTransactions;


public interface McqExamTransactionRepo extends JpaRepository<McqExamTransactions, Integer> {

}
