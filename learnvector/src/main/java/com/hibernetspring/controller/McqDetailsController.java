package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.McqDetails;
import com.hibernetspring.service.McqDetailService;


@RestController
@Path("mcqdeatil")
public class McqDetailsController {
	@Autowired
	private McqDetailService mcqDeatilService;
	@POST
	@Path("/addmcqdetail")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMcqDetail(@RequestBody McqDetails mcqDetail) {
		
		return mcqDeatilService.saveMcqDetail(mcqDetail);
		
	}
}