package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;

@Entity
@Table(name ="chapters")
public class Chapters {

	/*
	 
	 `chapter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chapter_name` varchar(100) DEFAULT NULL,
  `subject_id` int(11) NOT NULL COMMENT 'foreignkey',
  `icon_image_for_chapter` int(11) DEFAULT NULL COMMENT 'media_key_id',
  `intro_video_youtube_link` int(11) DEFAULT NULL COMMENT 'media_key_id',
  `number_of_subtopics_per_chapter` int(11) DEFAULT NULL COMMENT 'dynamic_cron',
  `video_hours` float DEFAULT NULL COMMENT 'dynamic_cron',
  `number_of_mcqs` int(11) DEFAULT NULL COMMENT 'dynamic total number of questions',
  `mcq_banks` int(11) DEFAULT NULL COMMENT 'dynamic_cron Calculated based on master_Attribute mcq_batch_size',
  `intro_text` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`chapter_id`)
	 
	 */
	
	
	@Id
	@Column(name="chapter_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int chapter_id;
	@Column(name="chapter_name")
	private String chapter_name;
	@Column(name="subject_id")
	private int subject_id;
	
	@Column(name="icon_image_for_chapter")
	private String icon_image_for_chapter;
	
	@Column(name="intro_video_youtube_link")
	private String intro_video_youtube_link;
	
	@Column(name="number_of_subtopics_per_chapter")
	private String number_of_subtopics_per_chapter;
	
	@Column(name="video_hours")
	private Float video_hours;
	
	@Column(name="number_of_mcqs")
	private int number_of_mcqs;
	
	@Column(name="mcq_banks")
	private int mcq_banks;
	@Column(name="intro_text")
	private int intro_text;
	
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;

	public int getChapter_id() {
		return chapter_id;
	}

	public void setChapter_id(int chapter_id) {
		this.chapter_id = chapter_id;
	}

	public String getChapter_name() {
		return chapter_name;
	}

	public void setChapter_name(String chapter_name) {
		this.chapter_name = chapter_name;
	}

	public int getSubject_id() {
		return subject_id;
	}

	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}

	public String getIcon_image_for_chapter() {
		return icon_image_for_chapter;
	}

	public void setIcon_image_for_chapter(String icon_image_for_chapter) {
		this.icon_image_for_chapter = icon_image_for_chapter;
	}

	public String getIntro_video_youtube_link() {
		return intro_video_youtube_link;
	}

	public void setIntro_video_youtube_link(String intro_video_youtube_link) {
		this.intro_video_youtube_link = intro_video_youtube_link;
	}

	public String getNumber_of_subtopics_per_chapter() {
		return number_of_subtopics_per_chapter;
	}

	public void setNumber_of_subtopics_per_chapter(String number_of_subtopics_per_chapter) {
		this.number_of_subtopics_per_chapter = number_of_subtopics_per_chapter;
	}

	public Float getVideo_hours() {
		return video_hours;
	}

	public void setVideo_hours(Float video_hours) {
		this.video_hours = video_hours;
	}

	public int getNumber_of_mcqs() {
		return number_of_mcqs;
	}

	public void setNumber_of_mcqs(int number_of_mcqs) {
		this.number_of_mcqs = number_of_mcqs;
	}

	public int getMcq_banks() {
		return mcq_banks;
	}

	public void setMcq_banks(int mcq_banks) {
		this.mcq_banks = mcq_banks;
	}

	public int getIntro_text() {
		return intro_text;
	}

	public void setIntro_text(int intro_text) {
		this.intro_text = intro_text;
	}

	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}

	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
	
	
	
	
	
	
	
	
	

	
}
