package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;

@Entity
@Table(name ="Broad_casting")
public class Broadcasting {
/*
  `broadcast_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firebase_api_token` varchar(250) DEFAULT NULL,
  `apple_api_token` varchar(250) DEFAULT NULL,
  `app_package_name` varchar(250) DEFAULT NULL,
  `app_type` enum('android','ios') DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`broadcast_id`)
 */
	
	
	
	@Id
	@Column(name="cid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int broadcast_id;
	@Column(name="firebase_api_token")
	private String firebase_api_token;
	
	@Column(name="apple_api_token")
	private String apple_api_token;
	
	@Column(name="app_package_name")
	private String app_package_name;
	
	
	
	//@Enumerated(EnumType.ORDINAL)
	//private AuthorStatus status;
	
	@Enumerated(EnumType.STRING)
    @Column(length = 8)
    private AppType app_type;
	  
	  
	  public enum AppType {
		    android,ios
		}
	  
		@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	  
	  
	  @Enumerated(EnumType.ORDINAL)
	    public AppType getApp_type() {
	        return app_type;
	    }


	public int getBroadcast_id() {
		return broadcast_id;
	}


	public void setBroadcast_id(int broadcast_id) {
		this.broadcast_id = broadcast_id;
	}


	public String getFirebase_api_token() {
		return firebase_api_token;
	}


	public void setFirebase_api_token(String firebase_api_token) {
		this.firebase_api_token = firebase_api_token;
	}


	public String getApple_api_token() {
		return apple_api_token;
	}


	public void setApple_api_token(String apple_api_token) {
		this.apple_api_token = apple_api_token;
	}


	public String getApp_package_name() {
		return app_package_name;
	}


	public void setApp_package_name(String app_package_name) {
		this.app_package_name = app_package_name;
	}


	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}


	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}


	public void setApp_type(AppType app_type) {
		this.app_type = app_type;
	}


	

}
