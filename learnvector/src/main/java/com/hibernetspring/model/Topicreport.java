package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="topicreport")
public class Topicreport {
	@Id
	@Column(name="trid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int trid;
	@Column(name="tid")
	int tid;

	@Column(name="usedrid")
	String usedrid;
	
	@Column(name="subjectid")
	int subjectid;
	
	@Column(name="subjectname")
	String subjectname;
	@Column(name="chapterid")
	int chapterid;
	@Column(name="chaptername")
	String chaptername;
	@Column(name="topicno")
	String topicno;
	@Column(name="topicname")
	String topicname;

	
	@Column(name="topicvideotime")
	String topicvideotime;
	
	
	@Column(name="ipaddress")
	String ipaddress;
	
	@Column(name="visitdatetime")
	String visitdatetime;

	public int getTrid() {
		return trid;
	}

	public void setTrid(int trid) {
		this.trid = trid;
	}

	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public String getUsedrid() {
		return usedrid;
	}

	public void setUsedrid(String usedrid) {
		this.usedrid = usedrid;
	}

	public int getSubjectid() {
		return subjectid;
	}

	public void setSubjectid(int subjectid) {
		this.subjectid = subjectid;
	}

	public String getSubjectname() {
		return subjectname;
	}

	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}

	public int getChapterid() {
		return chapterid;
	}

	public void setChapterid(int chapterid) {
		this.chapterid = chapterid;
	}

	public String getChaptername() {
		return chaptername;
	}

	public void setChaptername(String chaptername) {
		this.chaptername = chaptername;
	}

	public String getTopicno() {
		return topicno;
	}

	public void setTopicno(String topicno) {
		this.topicno = topicno;
	}

	public String getTopicname() {
		return topicname;
	}

	public void setTopicname(String topicname) {
		this.topicname = topicname;
	}



	public String getTopicvideotime() {
		return topicvideotime;
	}

	public void setTopicvideotime(String topicvideotime) {
		this.topicvideotime = topicvideotime;
	}

	public String getVisitdatetime() {
		return visitdatetime;
	}

	public void setVisitdatetime(String visitdatetime) {
		this.visitdatetime = visitdatetime;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	
	
	
	

}
