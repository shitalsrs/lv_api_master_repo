package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.McqDetails;
import com.hibernetspring.repository.McqDetailRepo;

@Service
public class McqDetailService {
	@Autowired
	private McqDetailRepo mcqDetailRepo;
	
	public Response saveMcqDetail(McqDetails mcqDetail) {
		
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(mcqDetailRepo.save(mcqDetail));		
			response.setSuccess(true);
			response.setMessage("Mcq detail record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create mcq detail record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
