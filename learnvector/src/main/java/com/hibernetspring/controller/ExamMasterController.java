package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.ExamMaster;
import com.hibernetspring.service.ExamMasterService;



@RestController
@Path("exammaster")
public class ExamMasterController {

	@Autowired
	private ExamMasterService examMasterService;
	
	@POST
	@Path("/addexammaster")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createLiveSession(@RequestBody ExamMaster examMaster) {
		
		return examMasterService.saveExamMaster(examMaster);
		
	
	}
}
