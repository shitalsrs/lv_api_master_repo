package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.hibernetspring.model1.SubjectGroupCode;

public interface SubjectGroupCodeRepo extends JpaRepository<SubjectGroupCode, Integer>{

}
