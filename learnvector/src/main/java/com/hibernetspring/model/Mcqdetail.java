package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mcqdetail")
public class Mcqdetail {

	@Id
	@Column(name = "mid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int mid;

	@Column(name = "qno")
	private int qno;

	@Column(name = "sid")
	private int sid;
	@Column(name = "chapterid")
	private String chapterid;
	
	@Column(name = "cfid")
	private String cfid;
	
	@Column(name = "subtopicid")
	private String subtopicid;	

	@Column(name = "question")
	private String question;

	@Column(name = "q_img")
	private String q_img;

	@Column(name = "option1")
	private String option1;

	@Column(name = "opt1_img")
	private String opt1_img;

	@Column(name = "option2")
	private String option2;

	@Column(name = "opt2_img")
	private String opt2_img;

	@Column(name = "option3")
	private String option3;

	@Column(name = "opt3_img")
	private String opt3_img;

	@Column(name = "option4")
	private String option4;

	@Column(name = "opt4_img")
	private String opt4_img;
	
	

	@Column(name = "ans_img")
	private String ans_img;
	
	@Column(name = "answer")
	private String answer;
	
	@Column(name = "mcq_type")
	private String mcq_type;
	
	@Column(name = "hint")
	private String hint;

	@Column(name = "hint_img")
	private String hint_img;

	@Column(name = "exp")
	private String exp;


	@Column(name = "exp_ref_vid")
	private String exp_ref_vid;
	
	@Column(name = "exp_img")
	private String exp_img;
	
	@Column(name = "nc11th")
	private String nc11th;

	@Column(name = "nc12th")
	private String nc12th;
	
	@Column(name = "cet")
	private String cet;
	
	@Column(name = "neet")
	private String neet;

	@Column(name = "jee")
	private String jee;

	

	

	@Column(name = "mc11th")
	private String mc11th;

	@Column(name = "mc12th")
	private String mc12th;

	@Column(name = "subject")
	private String subject;

	@Column(name = "chapter")
	private String chapter;

	@Column(name = "previous_exam")
	private String previous_exam;

	@Column(name = "q_level")
	private String q_level;

	@Column(name = "q_weigtage")
	private String q_weigtage;

	@Column(name = "ref_book")
	private String ref_book;
	@Column(name = "ref_vid")
	private String ref_vid;
	@Column(name = "submit")
	private String submit;

	@Column(name = "voice")
	private String voice;

	@Column(name = "whether_modified")
	private String whether_modified;

	

	@Column(name = "date")
	private String date;
	
	

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getQno() {
		return qno;
	}

	public void setQno(int qno) {
		this.qno = qno;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getChapter() {
		return chapter;
	}

	public void setChapter(String chapter) {
		this.chapter = chapter;
	}

	public String getChapterid() {
		return chapterid;
	}

	public void setChapterid(String chapterid) {
		this.chapterid = chapterid;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOpt1_img() {
		return opt1_img;
	}

	public void setOpt1_img(String opt1_img) {
		this.opt1_img = opt1_img;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOpt2_img() {
		return opt2_img;
	}

	public void setOpt2_img(String opt2_img) {
		this.opt2_img = opt2_img;
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public String getOpt3_img() {
		return opt3_img;
	}

	public void setOpt3_img(String opt3_img) {
		this.opt3_img = opt3_img;
	}

	public String getOption4() {
		return option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}

	public String getOpt4_img() {
		return opt4_img;
	}

	public void setOpt4_img(String opt4_img) {
		this.opt4_img = opt4_img;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getAns_img() {
		return ans_img;
	}

	public void setAns_img(String ans_img) {
		this.ans_img = ans_img;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public String getHint_img() {
		return hint_img;
	}

	public void setHint_img(String hint_img) {
		this.hint_img = hint_img;
	}

	public String getExp() {
		return exp;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}

	public String getExp_img() {
		return exp_img;
	}

	public void setExp_img(String exp_img) {
		this.exp_img = exp_img;
	}

	public String getExp_ref_vid() {
		return exp_ref_vid;
	}

	public void setExp_ref_vid(String exp_ref_vid) {
		this.exp_ref_vid = exp_ref_vid;
	}

	public String getVoice() {
		return voice;
	}

	public void setVoice(String voice) {
		this.voice = voice;
	}

	public String getNeet() {
		return neet;
	}

	public void setNeet(String neet) {
		this.neet = neet;
	}

	public String getJee() {
		return jee;
	}

	public void setJee(String jee) {
		this.jee = jee;
	}

	public String getCet() {
		return cet;
	}

	public void setCet(String cet) {
		this.cet = cet;
	}

	public String getNc11th() {
		return nc11th;
	}

	public void setNc11th(String nc11th) {
		this.nc11th = nc11th;
	}

	public String getNc12th() {
		return nc12th;
	}

	public void setNc12th(String nc12th) {
		this.nc12th = nc12th;
	}

	public String getMc11th() {
		return mc11th;
	}

	public void setMc11th(String mc11th) {
		this.mc11th = mc11th;
	}

	public String getMc12th() {
		return mc12th;
	}

	public void setMc12th(String mc12th) {
		this.mc12th = mc12th;
	}

	public String getMcq_type() {
		return mcq_type;
	}

	public void setMcq_type(String mcq_type) {
		this.mcq_type = mcq_type;
	}

	public String getPrevious_exam() {
		return previous_exam;
	}

	public void setPrevious_exam(String previous_exam) {
		this.previous_exam = previous_exam;
	}

	public String getQ_level() {
		return q_level;
	}

	public void setQ_level(String q_level) {
		this.q_level = q_level;
	}

	public String getQ_weigtage() {
		return q_weigtage;
	}

	public void setQ_weigtage(String q_weigtage) {
		this.q_weigtage = q_weigtage;
	}

	public String getRef_vid() {
		return ref_vid;
	}

	public void setRef_vid(String ref_vid) {
		this.ref_vid = ref_vid;
	}

	public String getRef_book() {
		return ref_book;
	}

	public void setRef_book(String ref_book) {
		this.ref_book = ref_book;
	}

	public String getWhether_modified() {
		return whether_modified;
	}

	public void setWhether_modified(String whether_modified) {
		this.whether_modified = whether_modified;
	}


	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getSubtopicid() {
		return subtopicid;
	}

	public void setSubtopicid(String subtopicid) {
		this.subtopicid = subtopicid;
	}

	public String getQ_img() {
		return q_img;
	}

	public void setQ_img(String q_img) {
		this.q_img = q_img;
	}

}
