package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name ="topic")
public class Topic {

	@Id
	@Column(name="tid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int tid;
	@Column(name="subjectid")
	int subjectid;
	@Column(name="chapterid")
	int chapterid;
	@Column(name="topicno")
	String topicno;
	@Column(name="topicname")
	String topicname;
	@Column(name="topicurl")
	String topicurl;
	
	@Column(name="topicvideotime")
	String topicvideotime;
	@Column(name="subtopic_name")
	String subtopic_name;
	@Column(name="subtopic_id")
	int subtopic_id;
	@Column(name="section")	
	String section;
	@Column(name="neet") 	
	String neet;
	@Column(name="jee")
	String jee;
	@Column(name="cet")	
	String cet;
	@Column(name="nc11th")	
	String nc11th;
	@Column(name="nc12th")	
	String nc12th;
	@Column(name="mc11th")
	String mc11th;
	@Column(name="mc12th")
	String mc12th;
	
	
	
	
	
	
	
	
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public int getChapterid() {
		return chapterid;
	}
	public void setChapterid(int chapterid) {
		this.chapterid = chapterid;
	}
	public String getTopicname() {
		return topicname;
	}
	public void setTopicname(String topicname) {
		this.topicname = topicname;
	}
	public String getTopicurl() {
		return topicurl;
	}
	public void setTopicurl(String topicurl) {
		this.topicurl = topicurl;
	}
	public int getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(int subjectid) {
		this.subjectid = subjectid;
	}
	public String getTopicno() {
		return topicno;
	}
	public void setTopicno(String topicno) {
		this.topicno = topicno;
	}
	public String getTopicvideotime() {
		return topicvideotime;
	}
	public void setTopicvideotime(String topicvideotime) {
		this.topicvideotime = topicvideotime;
	}
	public String getSubtopic_name() {
		return subtopic_name;
	}
	public void setSubtopic_name(String subtopic_name) {
		this.subtopic_name = subtopic_name;
	}
	public int getSubtopic_id() {
		return subtopic_id;
	}
	public void setSubtopic_id(int subtopic_id) {
		this.subtopic_id = subtopic_id;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getNeet() {
		return neet;
	}
	public void setNeet(String neet) {
		this.neet = neet;
	}
	public String getJee() {
		return jee;
	}
	public void setJee(String jee) {
		this.jee = jee;
	}
	public String getCet() {
		return cet;
	}
	public void setCet(String cet) {
		this.cet = cet;
	}
	public String getNc11th() {
		return nc11th;
	}
	public void setNc11th(String nc11th) {
		this.nc11th = nc11th;
	}
	public String getNc12th() {
		return nc12th;
	}
	public void setNc12th(String nc12th) {
		this.nc12th = nc12th;
	}
	public String getMc11th() {
		return mc11th;
	}
	public void setMc11th(String mc11th) {
		this.mc11th = mc11th;
	}
	public String getMc12th() {
		return mc12th;
	}
	public void setMc12th(String mc12th) {
		this.mc12th = mc12th;
	}
	
	
	
	
}
