package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.ChapterUpdatesPerReg;

public interface ChapterUpdateRepo extends JpaRepository<ChapterUpdatesPerReg, Integer> {

}
