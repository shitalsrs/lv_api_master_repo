package com.hibernetspring.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.Query;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import com.hibernetspring.model.Stock;
import com.hibernetspring.model.Trip;
import com.hibernetspring.model.Vehicaltripday;
import com.hibernetspring.model.Vehicaltripdaycount;
import com.hibernetspring.model.Vehicaltripmonths;


public class StockDAO {
	//SessionFactory sessionFactoryStock=null;
	//Session sessionStock=null;
	
	
	private static final String getsqltripday="select vehicaltrip.vt_id as vt_id,vehicaltrip.vt_entrydate as vt_entrydate,sum(vehicaltrip.vt_volume) as vt_volume from vehicaltrip group by vehicaltrip.vt_entrydate order by vehicaltrip.vt_id";
	
	public  StockDAO(){
		//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    	
	}
	
	public Stock saveStock(Stock stock)
	{
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=sessionFactoryStock.openSession();
			Session sessionStock=HibernateUtil.getSessionFactory().openSession();
			sessionStock.beginTransaction();
			sessionStock.save(stock);
			sessionStock.getTransaction().commit();
			sessionStock.close();
			//sessionFactoryStock.close();
			
	
		}catch(Exception e){
			e.printStackTrace();
			
			System.out.println("Session error"+e.getMessage());
			//sessionStock.getTransaction().rollback();
		}
		return stock;
	}
	
	

	public List<Stock> getStockList(String dates)
	{
		List<Stock> stock=new ArrayList<Stock>();
		Session sessionStock=HibernateUtil.getSessionFactory().openSession();
		try{
		
			String hql = "FROM Stock s where s.s_entrydate=:s_entrydate";
			
 
			Query query = sessionStock.createQuery(hql);
			query.setParameter("s_entrydate",dates);
			  System.out.println(dates+"");

			List results = ((org.hibernate.Query) query).list();
		
		    List<Stock> stocs=(ArrayList<Stock>)results;	
		     //System.out.println(stocs.get(0).toString());
		    return stocs;
			
		    
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionStock.close();
			//sessionFactoryStock.close();
		}
		return stock;
	}
	
	
	
	public List<Vehicaltripday> getStockListday(String dates)
	{
		List<Vehicaltripday> stock=new ArrayList<Vehicaltripday>();
		Session sessionStock=HibernateUtil.getSessionFactory().openSession();
		
		try{
			//List<Vehicaltripday> stocs = sessionStock.createQuery("SELECT v FROM Vehicaltripday v", Vehicaltripday.class).getResultList();
 			  String hql = "FROM Vehicaltripday vtd where vtd.vt_entrydate=:vt_entrydate";
			  Query query = sessionStock.createQuery(hql);
			  query.setParameter("vt_entrydate",dates); 
			  System.out.println(dates+""); 
			  List results = ((org.hibernate.Query) query).list(); 
			  List<Vehicaltripday> stocs=(ArrayList<Vehicaltripday>)results;
			
			  
			//System.out.println(stocs.get(0).toString());
		    return stocs;
			
		    
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionStock.close();
			//sessionFactoryStock.close();
		}
		return stock;
	}
	
	
	
	public List<Vehicaltripday> getStockListdaysount(String dates)
	{
		 List<Vehicaltripday> stock=new ArrayList<Vehicaltripday>();
		//Vehicaltripday stock=new Vehicaltripday();
		Session sessionStock=HibernateUtil.getSessionFactory().openSession();
			
		try{
			//List<Vehicaltripday> stocs = sessionStock.createQuery("SELECT v FROM Vehicaltripday v", Vehicaltripday.class).getResultList();
 			//  String hql = "FROM Vehicaltripday vtd where vtd.vt_entrydate=:vt_entrydate";
			  //Query query = sessionStock.createQuery(hql);
 			 Query query = sessionStock.createSQLQuery(getsqltripday);
			  //query.setParameter("vt_entrydate",dates); 
			  System.out.println(dates+""); 
			  List results = ((org.hibernate.Query) query).list(); 
			  List<Vehicaltripday> stocs=(ArrayList<Vehicaltripday>)results;
			  
			 //Vehicaltripday s=stocs.get(arg0);
			//System.out.println(stocs.get(0).toString());
		    return stocs;
			
		    
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionStock.close();
			//sessionFactoryStock.close();
		}
		return stock;
	}
	
	
	public List<Vehicaltripmonths> getStockListMonth(String months,String years)
	{
		List<Vehicaltripmonths> stockmonth=new ArrayList<Vehicaltripmonths>();
		Session sessionStock=HibernateUtil.getSessionFactory().openSession();
		
		try{
			 			  String hql = "FROM Vehicaltripmonths vtm where vtm.vt_month=:vt_month and vtm.vt_year=:vt_year";
			  Query query = sessionStock.createQuery(hql);
			  query.setParameter("vt_month",months); 
			  query.setParameter("vt_year",years); 
			  List results = ((org.hibernate.Query) query).list(); 
			  List<Vehicaltripmonths> stocs=(ArrayList<Vehicaltripmonths>)results;
			
			
		    return stocs;
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionStock.close();
			//sessionFactoryStock.close();
		}
		return stockmonth;
	}
	
	
	
	
	public Stock getStock(String dates)
	{
		Stock stock=new Stock();
		Session sessionStock=HibernateUtil.getSessionFactory().openSession();
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=HibernateUtil.getSessionFactory().openSession();
			
			//sessionStock = this.sessionFactoryStock.openSession();
			
			
			//String hql = "FROM Vehical AS Vehical";
			String hql = "FROM Stock s where s.s_entrydate=:s_entrydate";
 
			Query query = sessionStock.createQuery(hql);
			query.setParameter("s_entrydate",dates);
			  System.out.println(dates+"");

			List results = ((org.hibernate.Query) query).list();
			if(results.size()==0){
		    	  System.out.println(" get result"+results.size()+"--");
		    	  Stock getstocks=new Stock();
		    	  getstocks.setS_id(0);
		    	  getstocks.setS_entrydate("01-01-1111");
		    	  return getstocks;
		    }else{
			
		    List<Stock> stocs=(ArrayList<Stock>)results;	
		    Stock s=stocs.get(0);
		    Stock getstock =(Stock)sessionStock.get(Stock.class,s.getS_id()); 
		    return getstock;
		    }
		    
		    
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionStock.close();
		//	sessionFactoryStock.close();
		
		}
		return stock;
	}
	
	
	
	public Stock LastRecord()
	{
		
		
		Stock stock=new Stock();
		Session sessionStock=HibernateUtil.getSessionFactory().openSession();
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock = this.sessionFactoryStock.openSession();
			
			//String hql = "FROM Vehical AS Vehical";
			String hql = "FROM Stock s where s.s_id>=:s_id order by s.s_id desc";
     
			
		 int dates=1;
			Query query = sessionStock.createQuery(hql);
			query.setParameter("s_id",dates);
			  System.out.println(dates+"");

			List results = ((org.hibernate.Query) query).list();
		    List<Stock> stocs=(ArrayList<Stock>)results;	
		     //System.out.println(stocs.get(0).toString());
		    Stock s=stocs.get(0);
		    Stock getstock =(Stock)sessionStock.get(Stock.class,s.getS_id()); 
    
		    return getstock;
			
		    
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionStock.close();
			//sessionFactoryStock.close();
		}
		return stock;
	}
	
	
	public Boolean updateStockDao(Float Vt_volume,String Vt_entrydate,String datetime){
		
		//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		//Session sessionStockss = sessionFactoryStock.openSession();
		Session sessionStockss=HibernateUtil.getSessionFactory().openSession();
		
	      Transaction tx = null;
  try{
	         tx = sessionStockss.beginTransaction();
	         /*DriverN driverN =(DriverN)session.get(DriverN.class, Vt_entrydate); 
	         driverN.setDv_id(dv_id);
			 */
	        
	         
	         Stock getstock=getStock(Vt_entrydate);
	         String b2="pass";
	         System.out.println("Not today Date"+b2);
	         if(getstock.getS_entrydate().equals(Vt_entrydate)){
	         Double totalquantity=getstock.getS_totalquantity()+Vt_volume;
	         Float quantitymonth=getstock.getS_totalquantitymonth()+Vt_volume;
	         Float stockonjetty=getstock.getS_stockonjetty()-Vt_volume;
	         Float daycollection=getstock.getS_daycollect()+Vt_volume;
	         System.out.println("SELECTED DATA"+Vt_entrydate);
	         Stock stockforupdate =(Stock)sessionStockss.get(Stock.class,getstock.getS_id()); 
	         stockforupdate.setS_stockonjetty(stockonjetty);
	         stockforupdate.setS_totalquantity(totalquantity);
	         stockforupdate.setS_totalquantitymonth(quantitymonth);
	         stockforupdate.setS_daycollect(daycollection);
	         stockforupdate.setS_datetime(datetime);
	         sessionStockss.update(stockforupdate); 
	         }else{
	        	 System.out.println("--ELSE PART");
	        	/* DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);	        	
					Date date = format.parse(Vt_entrydate.toString());*/
	        	 StockDAO stockdao=new StockDAO();
	        	 Stock st=stockdao.LastRecord();
	        	 Double totalquantity=st.getS_totalquantity()+Vt_volume;
		         Float quantitymonth=st.getS_totalquantitymonth()+Vt_volume;
		         Float stockonjetty=st.getS_stockonjetty()-Vt_volume;
		         Float daycollection=Vt_volume; 
		       
		       System.out.println("--"+totalquantity+"---"+quantitymonth+"----"+daycollection);
		         Stock sto=new Stock();
		         
		         sto.setS_stockonjetty(stockonjetty);
		         sto.setS_totalquantity(totalquantity);
		         sto.setS_totalquantitymonth(quantitymonth);
		         sto.setS_daycollect(daycollection);
		         sto.setS_entrydate(Vt_entrydate);
		         sto.setS_datetime(datetime);		         		         
		         saveStock(sto);
	         }
	
	        tx.commit();
	         
	         return true;
     }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	         return false;
	      }finally {
	    	  sessionStockss.close(); 
	    	//  sessionFactoryStock.close();
	      }
		
	}
	
	
	
	
public Boolean updateStockDaoMinus(Float Vt_volume,String Vt_entrydate,String datetime){
		
		//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		//Session sessionStockss = sessionFactoryStock.openSession();
		Session sessionStockss=HibernateUtil.getSessionFactory().openSession();
		
	      Transaction tx = null;
  try{
	         tx = sessionStockss.beginTransaction();
	         /*DriverN driverN =(DriverN)session.get(DriverN.class, Vt_entrydate); 
	         driverN.setDv_id(dv_id);
			 */
	        
	         
	         Stock getstock=getStock(Vt_entrydate);
	         String b2="pass";
	         System.out.println("Not today Date"+b2);
	         if(getstock.getS_entrydate().equals(Vt_entrydate)){
	         Double totalquantity=getstock.getS_totalquantity()-Vt_volume;
	         Float quantitymonth=getstock.getS_totalquantitymonth()-Vt_volume;
	         Float stockonjetty=getstock.getS_stockonjetty()+Vt_volume;
	         Float daycollection=getstock.getS_daycollect()-Vt_volume;
	         System.out.println("SELECTED DATA"+Vt_entrydate);
	         Stock stockforupdate =(Stock)sessionStockss.get(Stock.class,getstock.getS_id()); 
	         stockforupdate.setS_stockonjetty(stockonjetty);
	         stockforupdate.setS_totalquantity(totalquantity);
	         stockforupdate.setS_totalquantitymonth(quantitymonth);
	         stockforupdate.setS_daycollect(daycollection);
	         stockforupdate.setS_datetime(datetime);
	         sessionStockss.update(stockforupdate); 
	         }else{
	        	 System.out.println("--ELSE PART");
	        	/* DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);	        	
					Date date = format.parse(Vt_entrydate.toString());*/
	        	 StockDAO stockdao=new StockDAO();
	        	 Stock st=stockdao.LastRecord();
	        	 Double totalquantity=st.getS_totalquantity()-Vt_volume;
		         Float quantitymonth=st.getS_totalquantitymonth()-Vt_volume;
		         Float stockonjetty=st.getS_stockonjetty()+Vt_volume;
		         Float daycollection=Vt_volume; 
		       
		       System.out.println("--"+totalquantity+"---"+quantitymonth+"----"+daycollection);
		         Stock sto=new Stock();
		         
		         sto.setS_stockonjetty(stockonjetty);
		         sto.setS_totalquantity(totalquantity);
		         sto.setS_totalquantitymonth(quantitymonth);
		         sto.setS_daycollect(daycollection);
		         sto.setS_entrydate(Vt_entrydate);
		         sto.setS_datetime(datetime);		         		         
		         saveStock(sto);
	         }
	
	        tx.commit();
	         
	         return true;
     }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	         return false;
	      }finally {
	    	  sessionStockss.close(); 
	    	//  sessionFactoryStock.close();
	      }
		
	}
	
	
	
	
	
	
	
	
public Boolean updateStockDaoJetty(Float Vt_volume,String Vt_entrydate,String datetime){
		
		//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		//Session sessionStockss = sessionFactoryStock.openSession();
		Session sessionStockss=HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
  try{
	         tx = sessionStockss.beginTransaction();
	         /*DriverN driverN =(DriverN)session.get(DriverN.class, Vt_entrydate); 
	         driverN.setDv_id(dv_id);
			 */
	        
	         //s_stockonjetty
	         Stock getstock=getStock(Vt_entrydate);
	         String b2="pass";
	         System.out.println("Not today Date"+b2);
	         if(getstock.getS_entrydate().equals(Vt_entrydate)){
	         Float stockonjetty=getstock.getS_stockonjetty()+Vt_volume;
	         
	         Double totalquantity=getstock.getS_totalquantity();
	         Float quantitymonth=getstock.getS_totalquantitymonth();
	         Float daycollection=(float) 0.00; 
	         
	         System.out.println("SELECTED DATA"+Vt_entrydate);
	         Stock stockforupdate =(Stock)sessionStockss.get(Stock.class,getstock.getS_id()); 
	         stockforupdate.setS_stockonjetty(stockonjetty);
	         
	         stockforupdate.setS_totalquantity(totalquantity);
	         stockforupdate.setS_totalquantitymonth(quantitymonth);
	         stockforupdate.setS_daycollect(daycollection);
	         
	         stockforupdate.setS_datetime(datetime);
	         sessionStockss.update(stockforupdate); 
	         }else{
	        	 System.out.println("--ELSE PART");
	        	/* DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);	        	
					Date date = format.parse(Vt_entrydate.toString());*/
	        	 StockDAO stockdao=new StockDAO();
	        	 Stock st=stockdao.LastRecord();
	        	 
	        	 Double totalquantity=st.getS_totalquantity();
		         Float quantitymonth=st.getS_totalquantitymonth();
		         Float daycollection=(float) 0.00; 
		         
		         Float stockonjetty=st.getS_stockonjetty()+Vt_volume;
		       
		       //System.out.println("--"+totalquantity+"---"+quantitymonth+"----"+daycollection);
		         Stock sto=new Stock();
		         sto.setS_stockonjetty(st.getS_stockonjetty());
		         
		         sto.setS_totalquantity(totalquantity);
		         sto.setS_totalquantitymonth(quantitymonth);
		         sto.setS_daycollect(daycollection);
		         
		         sto.setS_stockonjetty(stockonjetty);
		         sto.setS_entrydate(Vt_entrydate);
		         sto.setS_datetime(datetime);		         		         
		         saveStock(sto);
	         }
	
	        tx.commit();
	         
	         return true;
     }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	         return false;
	      }finally {
	    	  sessionStockss.close(); 
	    	 // sessionFactoryStock.close();
	      }
		
	}
	
}
