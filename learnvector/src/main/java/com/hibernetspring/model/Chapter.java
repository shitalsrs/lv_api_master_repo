package com.hibernetspring.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name ="chapter")
public class Chapter {

	@Id
	@Column(name="cid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int cid;
	@Column(name="cno")
	int cno;
	@Column(name="cname")
	String cname;
	@Column(name="subid")
	int subid;
	@Column(name="subname")
	String subname;
	@Column(name="totalqueston")
	int totalqueston;
	@Column(name="chapterdesc")
	String chapterdesc;
	@Column(name="updatedatetime")
	String updatedatetime;
	

   // private List<StudMcq> mcqdata;
	
	/* @Column(name="topiclist") private List<Topic> topicdata; */
	 
	
	
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public String getSubname() {
		return subname;
	}
	public void setSubname(String subname) {
		this.subname = subname;
	}

	
	/*
	 * public List<Topic> getTopicdata() { return topicdata; } public void
	 * setTopicdata(List<Topic> topicdata) { this.topicdata = topicdata; }
	 */
	 
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public int getSubid() {
		return subid;
	}
	public void setSubid(int subid) {
		this.subid = subid;
	}
	public int getTotalqueston() {
		return totalqueston;
	}
	public void setTotalqueston(int totalqueston) {
		this.totalqueston = totalqueston;
	}
	public String getChapterdesc() {
		return chapterdesc;
	}
	public void setChapterdesc(String chapterdesc) {
		this.chapterdesc = chapterdesc;
	}
	public String getUpdatedatetime() {
		return updatedatetime;
	}
	public void setUpdatedatetime(String updatedatetime) {
		this.updatedatetime = updatedatetime;
	} 
	
	
	
	
	

	
}
