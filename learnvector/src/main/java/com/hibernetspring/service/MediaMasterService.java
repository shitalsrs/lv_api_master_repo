package com.hibernetspring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.MediaMaster;
import com.hibernetspring.repository.MediaMasterRepository;
import javax.ws.rs.core.Response;
@Service
public class MediaMasterService {

	@Autowired
	private MediaMasterRepository mediaMasterRepo;
	
	public Response saveMediaMater(MediaMaster mediaMaster) {
		//return mediaMasterRepo.save(mediaMaster);
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(mediaMasterRepo.save(mediaMaster));		
			response.setSuccess(true);
			response.setMessage("Media Master record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create Media Master record, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
	
}
