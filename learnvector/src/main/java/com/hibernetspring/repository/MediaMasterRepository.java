package com.hibernetspring.repository;
import com.hibernetspring.model1.MediaMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaMasterRepository extends JpaRepository<MediaMaster,Integer> {
	
	MediaMaster findByMedia_id(int id);


}
