package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable 
public class Chapterlist {
	@Column(name="cid")
	   private int cid; 

	

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}
	   
	public String toString()
	{
	return "{cid: "+cid+"}";
	}
	   
}
