package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hibernetspring.model1.StudentMQChaptPurchased;

public interface StudentMQChaptPurchasedRepo extends JpaRepository<StudentMQChaptPurchased, Integer> {

}
