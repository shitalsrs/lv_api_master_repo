package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.LoginHistory;

public interface LoginHistoryRepo extends JpaRepository<LoginHistory, Integer>{

}
