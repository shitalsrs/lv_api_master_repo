package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="mcq_details")
public class McqDetails {

/*
  `mcq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mcq_record_question_id` int(11) DEFAULT NULL,
  `mcq_record_answer_id` int(11) DEFAULT NULL,
  `mcq_record_hint_id` int(11) DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `mcq_choice_1_record_id` int(11) DEFAULT NULL,
  `mcq_choice_2_record_id` int(11) DEFAULT NULL,
  `mcq_choice_3_record_id` int(11) DEFAULT NULL,
  `mcq_choice_4_record_id` int(11) DEFAULT NULL,
  `marks` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`mcq_id`)
 */
	
	@Id
	@Column(name="mcq_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer mcq_id;
	@Column(name="mcq_record_question_id")
	private int mcq_record_question_id;
	@Column(name="mcq_record_answer_id")
	private int mcq_record_answer_id;
	@Column(name="mcq_record_hint_id")
	private int mcq_record_hint_id;
	@Column(name="chapter_id")
	private int chapter_id;
	@Column(name="mcq_choice_1_record_id")
	private int mcq_choice_1_record_id;
	@Column(name="mcq_choice_2_record_id")
	private int mcq_choice_2_record_id;
	@Column(name="mcq_choice_3_record_id")
	private int mcq_choice_3_record_id;
	@Column(name="mcq_choice_4_record_id")
	private int mcq_choice_4_record_id;
	@Column(name="marks")
	private int marks;
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	
	public int getMcq_id() {
		return mcq_id;
	}
	public void setMcq_id(int mcq_id) {
		this.mcq_id = mcq_id;
	}
	public int getMcq_record_question_id() {
		return mcq_record_question_id;
	}
	public void setMcq_record_question_id(int mcq_record_question_id) {
		this.mcq_record_question_id = mcq_record_question_id;
	}
	public int getMcq_record_answer_id() {
		return mcq_record_answer_id;
	}
	public void setMcq_record_answer_id(int mcq_record_answer_id) {
		this.mcq_record_answer_id = mcq_record_answer_id;
	}
	public int getMcq_record_hint_id() {
		return mcq_record_hint_id;
	}
	public void setMcq_record_hint_id(int mcq_record_hint_id) {
		this.mcq_record_hint_id = mcq_record_hint_id;
	}
	public int getChapter_id() {
		return chapter_id;
	}
	public void setChapter_id(int chapter_id) {
		this.chapter_id = chapter_id;
	}
	public int getMcq_choice_1_record_id() {
		return mcq_choice_1_record_id;
	}
	public void setMcq_choice_1_record_id(int mcq_choice_1_record_id) {
		this.mcq_choice_1_record_id = mcq_choice_1_record_id;
	}
	public int getMcq_choice_2_record_id() {
		return mcq_choice_2_record_id;
	}
	public void setMcq_choice_2_record_id(int mcq_choice_2_record_id) {
		this.mcq_choice_2_record_id = mcq_choice_2_record_id;
	}
	public int getMcq_choice_3_record_id() {
		return mcq_choice_3_record_id;
	}
	public void setMcq_choice_3_record_id(int mcq_choice_3_record_id) {
		this.mcq_choice_3_record_id = mcq_choice_3_record_id;
	}
	public int getMcq_choice_4_record_id() {
		return mcq_choice_4_record_id;
	}
	public void setMcq_choice_4_record_id(int mcq_choice_4_record_id) {
		this.mcq_choice_4_record_id = mcq_choice_4_record_id;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	 
}
