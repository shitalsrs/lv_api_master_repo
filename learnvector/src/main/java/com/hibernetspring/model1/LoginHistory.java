package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;

@Entity
@Table(name ="login_history")
public class LoginHistory {

	/*
	   `session_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logintoken` bigint(20) NOT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  `login_server_details` varchar(200) DEFAULT NULL,
  `current_login_analytics` text DEFAULT NULL COMMENT 'this is json with lots of fields reflecting last visited pages etc',
  `user_name` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `credential_type` enum('facebook','linkedin','gmail','plain') DEFAULT NULL,
  `certificates` text DEFAULT NULL,
  `more_login_info` text DEFAULT NULL,
  `session_active` tinyint(1) DEFAULT NULL COMMENT 'true false',
  `user_type` enum('student','admin','master_admin','teacher') DEFAULT NULL,
  `device_info` text DEFAULT NULL,
  `live_id` int(11) DEFAULT NULL COMMENT 'if any',
  `deleted` tinyint(1) DEFAULT NULL,
  `login_type` enum('live','standard') DEFAULT NULL,
  PRIMARY KEY (`session_id`)

	 
	 
	 */
	
	
	
	
	
	

	@Id
	@Column(name="session_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int session_id;
	
	@Column(name="logintoken")
	private int logintoken;
	
	@Column(name="registration_id")
	private int registration_id;
	
	@Column(name="login_time")
	private DateTime login_time;
	
	@Column(name="logout_time")
	private DateTime logout_time;
	
	@Column(name="login_server_details")
	private String login_server_details;
	
	@Column(columnDefinition="TEXT")
	private String current_login_analytics;
	
	@Column(name="user_name")
	private String user_name;
	
	@Column(name="password")
	private String password;
	
	@Enumerated(EnumType.STRING)
    @Column(length = 15)
    private credentialType credential_type;
	
	public enum credentialType {
		  gmail,linkedin,plain,facebook
		}
	  
	@Column(columnDefinition="TEXT")
	private String certificates;
	
	@Column(columnDefinition="TEXT")
	private String more_login_info;
	
	@Column(name="session_active")
	private TinyIntTypeDescriptor session_active;
	
	@Enumerated(EnumType.STRING)
    @Column(length = 15)
    private userType user_type;
	
	public enum userType {
		student,admin,master_admin,teacher
		}
	  
	@Column(columnDefinition="TEXT")
	private String device_info;
	
	  
	@Column(columnDefinition="TEXT")
	private int live_id;
	
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	
	 
	@Enumerated(EnumType.STRING)
    @Column(length = 15)
    private loginType login_type;
	
	public enum loginType {
		live,standard
		}
	 
}
