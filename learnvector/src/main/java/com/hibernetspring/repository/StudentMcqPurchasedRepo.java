package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.StudentMcqPurchased;

public interface StudentMcqPurchasedRepo extends JpaRepository<StudentMcqPurchased, Integer> {

}
