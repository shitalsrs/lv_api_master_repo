package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.Students;
import com.hibernetspring.service.StudentService;


@RestController
@Path("student")
public class StudentsController {
	@Autowired
	private StudentService studentService;
	
	//add StudChaptPurchased rest api
	
	@POST
	@Path("/addstudent")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createStudent(@RequestBody Students student) {
		
		return studentService.saveStudents(student);
		
	}
}