package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.Subtopics;

import com.hibernetspring.service.SubtopicService;



@RestController
@Path("subtopics")
public class SubtopicsController {
	@Autowired
	private SubtopicService subtopicService;
	
	
	
	@POST
	@Path("/addsubtopic")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createSubtopic(@RequestBody Subtopics subTopic) {
		
		return subtopicService.saveSubtopic(subTopic);
		
	}
}