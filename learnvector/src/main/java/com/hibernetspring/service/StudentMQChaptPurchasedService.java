package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.StudentMQChaptPurchased;
import com.hibernetspring.repository.StudentMQChaptPurchasedRepo;

@Service
public class StudentMQChaptPurchasedService {

	@Autowired
	private StudentMQChaptPurchasedRepo studentMQChaptPurchasedRepo;
public Response saveStudentMQChaptPurchased(StudentMQChaptPurchased studentMQChaptPurchased) {
		
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(studentMQChaptPurchasedRepo.save(studentMQChaptPurchased));		
			response.setSuccess(true);
			response.setMessage("student mcq cahpaterwise record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create student mcq cahpaterwise record, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}

