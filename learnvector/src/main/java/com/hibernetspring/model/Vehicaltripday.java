package com.hibernetspring.model;

import javax.persistence.Access;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jgroups.annotations.Immutable;

//Add query in view update database lavel 
//select `trip`.`vehicaltrip`.`vt_id` AS `vt_id`,`trip`.`vehicaltrip`.`vt_entrydate` AS `vt_entrydate`,sum(`trip`.`vehicaltrip`.`vt_volume`) AS `vt_volume` from `trip`.`vehicaltrip` group by `trip`.`vehicaltrip`.`vt_entrydate` order by `trip`.`vehicaltrip`.`vt_id`
//CREATE VIEW vehicaltripday AS select `tripload`.`vehicaltrip`.`vt_id` AS `vt_id`,`tripload`.`vehicaltrip`.`vt_entrydate` AS `vt_entrydate`,sum(`tripload`.`vehicaltrip`.`vt_volume`) AS `vt_volume` from `tripload`.`vehicaltrip` group by `tripload`.`vehicaltrip`.`vt_entrydate` order by `tripload`.`vehicaltrip`.`vt_id`

//http://timezra.blogspot.com/2009/05/mapping-hibernate-entities-to-views.html

@Entity
@Immutable
@Table(name="vehicaltripday")
public class Vehicaltripday {
	
	@Id
	@Column(name="vt_id")
	private int vt_id;
	
	@Column(name="vt_entrydate")
    private String vt_entrydate;
	
	@Column(name="vt_volume")
	private Double vt_volume;

	public int getVt_id() {
		return vt_id;
	}

	public void setVt_id(int vt_id) {
		this.vt_id = vt_id;
	}

	public String getVt_entrydate() {
		return vt_entrydate;
	}

	public void setVt_entrydate(String vt_entrydate) {
		this.vt_entrydate = vt_entrydate;
	}

	public Double getVt_volume() {
		return vt_volume;
	}

	public void setVt_volume(Double vt_volume) {
		this.vt_volume = vt_volume;
	}
	
	
	

}
