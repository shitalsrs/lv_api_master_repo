package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.McqMaster;
import com.hibernetspring.service.McqMasterService;




@RestController
@Path("mcqmaster")
public class McqMasterController {
	@Autowired
	private McqMasterService mcqMasterService;
	
	
	
	@POST
	@Path("/addmcqmaster")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMcqMaster(@RequestBody McqMaster mcqMaster) {
		
		return mcqMasterService.saveMcqMaster(mcqMaster);
		
	}
}

