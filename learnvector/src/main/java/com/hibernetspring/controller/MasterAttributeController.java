package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.MasterAttributes;
import com.hibernetspring.service.MasterAttributeService;

@RestController
@Path("masterattribute")
public class MasterAttributeController {
	@Autowired
	private MasterAttributeService masterAttributeService;
	
	
	@POST
	@Path("/addmasterattribute")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMaster(@RequestBody MasterAttributes masterAttributes) {
		
		return masterAttributeService.saveMasterAttribute(masterAttributes);
		
	}
}