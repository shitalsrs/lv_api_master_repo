package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

//Add query in view update database lavel 
//select `trip`.`vehicaltrip`.`vt_id` AS `vt_id`,`trip`.`vehicaltrip`.`vt_entrydate` AS `vt_entrydate`,sum(`trip`.`vehicaltrip`.`vt_volume`) AS `vt_volume` from `trip`.`vehicaltrip` group by `trip`.`vehicaltrip`.`vt_entrydate` order by `trip`.`vehicaltrip`.`vt_id`


@Entity
@Table(name="vehicaltrip")
public class Vehicaltrip {

	@Id
	@Column(name="vt_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int vt_id;
	
	@PrePersist
	public void prePersist() {
	    if(vt_type == null) //We set default value in case if the value is not set yet.
	    	vt_type = "0";
	}
	
	
	@Column(name="vt_number")
    private int vt_number;
	
	@Column(name="vt_gweight")
    private int vt_gweight;
	
	@Column(name="vt_eweight")
    private int vt_eweight;
	
	@Column(name="vt_nweight")
    private int vt_nweight;
	
	@Column(name="vt_volume")
    private Float vt_volume;

	@Column(name="vt_type")
    private String vt_type;
	
	@Column(name="vt_entrydate")
    private String vt_entrydate;
	
	@Column(name="vt_datetime")
    private String vt_datetime;

	public int getVt_id() {
		return vt_id;
	}

	public void setVt_id(int vt_id) {
		this.vt_id = vt_id;
	}

	public int getVt_number() {
		return vt_number;
	}

	public void setVt_number(int vt_number) {
		this.vt_number = vt_number;
	}

	public int getVt_gweight() {
		return vt_gweight;
	}

	public void setVt_gweight(int vt_gweight) {
		this.vt_gweight = vt_gweight;
	}

	public int getVt_eweight() {
		return vt_eweight;
	}

	public void setVt_eweight(int vt_eweight) {
		this.vt_eweight = vt_eweight;
	}
	
	
	

	public String getVt_type() {
		return vt_type;
	}

	public void setVt_type(String vt_type) {
		this.vt_type = vt_type;
	}

	public String getVt_entrydate() {
		return vt_entrydate;
	}

	public void setVt_entrydate(String vt_entrydate) {
		this.vt_entrydate = vt_entrydate;
	}

	public String getVt_datetime() {
		return vt_datetime;
	}

	public void setVt_datetime(String vt_datetime) {
		this.vt_datetime = vt_datetime;
	}

	
	public int getVt_nweight() {
		return vt_nweight;
	}

	public void setVt_nweight(int vt_nweight) {
		this.vt_nweight = vt_nweight;
	}

	public Float getVt_volume() {
		return vt_volume;
	}

	public void setVt_volume(Float vt_volume) {
		this.vt_volume = vt_volume;
	}
	
	
	
}
