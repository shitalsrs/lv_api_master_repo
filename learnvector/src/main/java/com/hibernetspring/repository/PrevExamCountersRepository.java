package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.PrevExamCounters;

public interface PrevExamCountersRepository extends JpaRepository<PrevExamCounters, Integer> {

}
