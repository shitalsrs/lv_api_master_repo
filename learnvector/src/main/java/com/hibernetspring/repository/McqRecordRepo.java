package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.McqRecord;


public interface McqRecordRepo extends JpaRepository<McqRecord,Integer> {
	
	
}