package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hibernetspring.model1.McqlistStudentwise;
import com.hibernetspring.service.McqListStudentWiseService;



@RestController
@Path("mcqliststudentwise")
public class McqlistStudentwiseController {
	
	@Autowired
	private McqListStudentWiseService mcqListStudentWiseService;
	
	@POST
	@Path("/addmcqstudent")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMcqStudentwise(@RequestBody McqlistStudentwise mcqStudentWise) {
		
		return mcqListStudentWiseService.saveMcqListStudent(mcqStudentWise);
		
	}
}