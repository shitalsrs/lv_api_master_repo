package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.hibernetspring.model1.StudentExaPurchased;

public interface StudentExamPurchasedRepo extends JpaRepository<StudentExaPurchased,Integer> {

}
