package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stock")
public class Stock {

	@Id
	@Column(name="s_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int s_id;
	
	@Column(name="s_stockonjetty")
    private Float s_stockonjetty;

	@Column(name="s_totalquantity")
   private Double s_totalquantity;
	
	@Column(name="s_totalquantitymonth")
    private Float s_totalquantitymonth;
	
	@Column(name="s_daycollect")
    private Float s_daycollect;
	
	@Column(name="s_entrydate")
    private String s_entrydate;
	
	@Column(name="s_datetime")
	private String s_datetime;

	public int getS_id() {
		return s_id;
	}

	public void setS_id(int s_id) {
		this.s_id = s_id;
	}

	public Float getS_stockonjetty() {
		return s_stockonjetty;
	}

	public void setS_stockonjetty(Float s_stockonjetty) {
		this.s_stockonjetty = s_stockonjetty;
	}

	public Double getS_totalquantity() {
		return s_totalquantity;
	}

	public void setS_totalquantity(Double s_totalquantity) {
		this.s_totalquantity = s_totalquantity;
	}

	public Float getS_totalquantitymonth() {
		return s_totalquantitymonth;
	}

	public void setS_totalquantitymonth(Float s_totalquantitymonth) {
		this.s_totalquantitymonth = s_totalquantitymonth;
	}

	public Float getS_daycollect() {
		return s_daycollect;
	}

	public void setS_daycollect(Float s_daycollect) {
		this.s_daycollect = s_daycollect;
	}

	public String getS_entrydate() {
		return s_entrydate;
	}

	public void setS_entrydate(String s_entrydate) {
		this.s_entrydate = s_entrydate;
	}

	public String getS_datetime() {
		return s_datetime;
	}

	public void setS_datetime(String s_datetime) {
		this.s_datetime = s_datetime;
	}
	
}
