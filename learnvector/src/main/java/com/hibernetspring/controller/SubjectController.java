package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hibernetspring.model1.Subjects;
import com.hibernetspring.service.SubjectService;
@RestController
@Path("subjects")
public class SubjectController {
	@Autowired
	private SubjectService subjectService;
	
	
	// add new subject api 
	@POST
	@Path("/addsubject")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createSubjects(@RequestBody Subjects subject) {
		
		return subjectService.savesubject(subject);
		
	}
}
