package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hibernetspring.model1.MediaMaster;
import com.hibernetspring.service.MediaMasterService;
import javax.ws.rs.core.Response;

@RestController
@Path("mediamaster")
public class MediaMasterController {
	
	@Autowired
	private MediaMasterService mediamasterService;
	
	@POST
	@Path("/addmediamaster")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMediaMaster(@RequestBody MediaMaster mediaMaster) {
		
		return mediamasterService.saveMediaMater(mediaMaster);
		
	}
}
