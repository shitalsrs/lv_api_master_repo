package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;

@Entity
@Table(name ="student_chapter_purchased")
public class StudentChapterPurchased {
/*
  `student_chapter_purchase_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `student_course_purchase_id` int(11) DEFAULT NULL COMMENT 'means this is part of package purchase',
  `course_id` int(11) DEFAULT NULL COMMENT 'if any',
  `purchase_date` datetime DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `purchased_chapter_version` int(11) DEFAULT NULL,
  `last_allowed_chapter_update_version` int(11) DEFAULT NULL COMMENT '0 for no restriction',
  `is_update_allowed` enum('allowed','not-allowed','upto-version') DEFAULT NULL,
  `update_not_allowed_reason` varchar(200) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
 */
	@Id
	@Column(name="student_chapter_purchase_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int student_chapter_purchase_id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="chapter_id")
	private int chapter_id;
	@Column(name="student_course_purchase_id")
	private int student_course_purchase_id;
	@Column(name="course_id")
	private int course_id;
	@Column(name="course_id")
	private DateTime purchase_date;
	
	  @Column(name="transaction_id")
		private int transaction_id;
		@Column(name="purchased_chapter_version")
		private int purchased_chapter_version;
		@Column(name="last_allowed_chapter_update_version")
		private int last_allowed_chapter_update_version;
		
		@Enumerated(EnumType.STRING)
		@Column(length = 8)
		private isUpdateAllowed is_update_allowed;
		  public enum isUpdateAllowed {
			  allowed,not_allowed,upto_version
			}
		
	 
	  @Column(name="update_not_allowed_reason")
		private String update_not_allowed_reason;
	  
	  @Column(name="expiry_date")
	  private DateTime expiry_date;
	  
	  @Column(name="deleted")
		private TinyIntTypeDescriptor deleted;

	public int getStudent_chapter_purchase_id() {
		return student_chapter_purchase_id;
	}

	public void setStudent_chapter_purchase_id(int student_chapter_purchase_id) {
		this.student_chapter_purchase_id = student_chapter_purchase_id;
	}

	public int getStudent_id() {
		return student_id;
	}

	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}

	public int getChapter_id() {
		return chapter_id;
	}

	public void setChapter_id(int chapter_id) {
		this.chapter_id = chapter_id;
	}

	public int getStudent_course_purchase_id() {
		return student_course_purchase_id;
	}

	public void setStudent_course_purchase_id(int student_course_purchase_id) {
		this.student_course_purchase_id = student_course_purchase_id;
	}

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public DateTime getPurchase_date() {
		return purchase_date;
	}

	public void setPurchase_date(DateTime purchase_date) {
		this.purchase_date = purchase_date;
	}

	public int getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}

	public int getPurchased_chapter_version() {
		return purchased_chapter_version;
	}

	public void setPurchased_chapter_version(int purchased_chapter_version) {
		this.purchased_chapter_version = purchased_chapter_version;
	}

	public int getLast_allowed_chapter_update_version() {
		return last_allowed_chapter_update_version;
	}

	public void setLast_allowed_chapter_update_version(int last_allowed_chapter_update_version) {
		this.last_allowed_chapter_update_version = last_allowed_chapter_update_version;
	}

	public isUpdateAllowed getIs_update_allowed() {
		return is_update_allowed;
	}

	public void setIs_update_allowed(isUpdateAllowed is_update_allowed) {
		this.is_update_allowed = is_update_allowed;
	}

	public String getUpdate_not_allowed_reason() {
		return update_not_allowed_reason;
	}

	public void setUpdate_not_allowed_reason(String update_not_allowed_reason) {
		this.update_not_allowed_reason = update_not_allowed_reason;
	}

	public DateTime getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(DateTime expiry_date) {
		this.expiry_date = expiry_date;
	}

	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}

	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	  
	  
}
