package com.hibernetspring.model;

import java.util.List;





public class ChapterTopic {

	private int cid;
	private  int cno;
	private String cname;
	private int subid;
	private String subname;
	private  int totalqueston;
	private String chapterdesc;
	private String updatedatetime;
	
	private  List<Topic> topic;

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public int getSubid() {
		return subid;
	}

	public void setSubid(int subid) {
		this.subid = subid;
	}

	public String getSubname() {
		return subname;
	}

	public void setSubname(String subname) {
		this.subname = subname;
	}

	public int getTotalqueston() {
		return totalqueston;
	}

	public void setTotalqueston(int totalqueston) {
		this.totalqueston = totalqueston;
	}

	public String getChapterdesc() {
		return chapterdesc;
	}

	public void setChapterdesc(String chapterdesc) {
		this.chapterdesc = chapterdesc;
	}

	public String getUpdatedatetime() {
		return updatedatetime;
	}

	public void setUpdatedatetime(String updatedatetime) {
		this.updatedatetime = updatedatetime;
	}

	public List<Topic> getTopic() {
		return topic;
	}

	public void setTopic(List<Topic> topic) {
		this.topic = topic;
	}
	
	
	
}
