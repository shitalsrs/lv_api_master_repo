package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.McqBankTransactions;

public interface McqBankRepo extends JpaRepository<McqBankTransactions, Integer> {

}
