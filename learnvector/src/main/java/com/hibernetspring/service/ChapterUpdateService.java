package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.ChapterUpdatesPerReg;
import com.hibernetspring.repository.ChapterUpdateRepo;


@Service
public class ChapterUpdateService {

	@Autowired
	private ChapterUpdateRepo chapterUpdateRepo;
	
	public Response saveChapterUpdate(ChapterUpdatesPerReg chapterUpdatesPerReg) {
		
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(chapterUpdateRepo.save(chapterUpdatesPerReg));		
			response.setSuccess(true);
			response.setMessage("Chapter update per registration map record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create chapter update per registration record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
