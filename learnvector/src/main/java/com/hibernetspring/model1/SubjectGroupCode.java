package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="subject_groupcode")
public class SubjectGroupCode {
/*
  `mapping_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_code_id` int(11) DEFAULT NULL,
  `group_code_value` varchar(500) DEFAULT NULL COMMENT 'e.g. PCB OR PCMB',
  `group_code_subjects` varchar(500) DEFAULT NULL COMMENT 'subID-subID-subID',
  `deleted` tinyint(1) DEFAULT NULL,
 */
	@Id
	@Column(name="mapping_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int mapping_id;
	@Column(name="group_code_id")
	private int group_code_id;
	@Column(name="group_code_value")
	private String group_code_value;
	@Column(name="group_code_subjects")
	private String group_code_subjects;
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	
	public int getMapping_id() {
		return mapping_id;
	}
	public void setMapping_id(int mapping_id) {
		this.mapping_id = mapping_id;
	}
	public int getGroup_code_id() {
		return group_code_id;
	}
	public void setGroup_code_id(int group_code_id) {
		this.group_code_id = group_code_id;
	}
	public String getGroup_code_value() {
		return group_code_value;
	}
	public void setGroup_code_value(String group_code_value) {
		this.group_code_value = group_code_value;
	}
	public String getGroup_code_subjects() {
		return group_code_subjects;
	}
	public void setGroup_code_subjects(String group_code_subjects) {
		this.group_code_subjects = group_code_subjects;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
	
	
	
	
	
}
