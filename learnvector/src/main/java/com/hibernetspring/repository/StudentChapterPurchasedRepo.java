package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.StudentChapterPurchased;

public interface StudentChapterPurchasedRepo extends JpaRepository<StudentChapterPurchased, Integer> {

}
