package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.VersionInfo;
import com.hibernetspring.service.VersionInfoService;


@RestController
@Path("versioninfo")
public class VersionInfoController {
	@Autowired
	private VersionInfoService versionInfoService;
	
	
	
	@POST
	@Path("/addversion")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createVersion(@RequestBody VersionInfo versionInfo) {
		
		return versionInfoService.saveVersion(versionInfo);
		
	}
}