package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="message")
public class Message {
	
	@Id
	@Column(name="mssageid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int mssageid;
	@Column(name="message")
	String message;
	@Column(name="userid")
	String userid;
	@Column(name="updatetime")
	String updatedatetime;
	@Column(name="visiteduserid")
	String visiteduserid;
	

	
}
