package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="version_info")
public class VersionInfo {
	/*`version_info_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `version_master_name` varchar(100) DEFAULT NULL COMMENT 'e.g. chapter, student, chapter',
	  `version_number` int(11) DEFAULT NULL,
	  `last_change` datetime DEFAULT NULL,
	  `last_change_by_user` int(11) DEFAULT NULL,
	  `last_change_id` int(11) DEFAULT NULL,
	  `deleted` tinyint(1) DEFAULT NULL,
	  */
	 @Id
	@Column(name="version_info_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int version_info_id;
	@Column(name="version_master_name")
	private String version_master_name;
	@Column(name="version_number")
	private int version_number;
	@Column(name="version_number")
	private DateTime last_change;
	  @Column(name="last_change_by_user")
		private int last_change_by_user;
	  @Column(name="last_change_id")
		private int last_change_id;
	  @Column(name="deleted")
		private TinyIntTypeDescriptor deleted;
	public int getVersion_info_id() {
		return version_info_id;
	}
	public void setVersion_info_id(int version_info_id) {
		this.version_info_id = version_info_id;
	}
	public String getVersion_master_name() {
		return version_master_name;
	}
	public void setVersion_master_name(String version_master_name) {
		this.version_master_name = version_master_name;
	}
	public int getVersion_number() {
		return version_number;
	}
	public void setVersion_number(int version_number) {
		this.version_number = version_number;
	}
	public DateTime getLast_change() {
		return last_change;
	}
	public void setLast_change(DateTime last_change) {
		this.last_change = last_change;
	}
	public int getLast_change_by_user() {
		return last_change_by_user;
	}
	public void setLast_change_by_user(int last_change_by_user) {
		this.last_change_by_user = last_change_by_user;
	}
	public int getLast_change_id() {
		return last_change_id;
	}
	public void setLast_change_id(int last_change_id) {
		this.last_change_id = last_change_id;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	  
	  
}
