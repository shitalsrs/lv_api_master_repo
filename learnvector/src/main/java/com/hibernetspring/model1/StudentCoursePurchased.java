package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="student_course_purchased")
public class StudentCoursePurchased {
/*
 `student_course_purchase_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `purchase_date` datetime DEFAULT NULL,
  `transaction_id` varchar(150) DEFAULT NULL,
  `purchase_course_version` int(11) DEFAULT NULL,
  `last_allowed_update_course_version` int(11) DEFAULT NULL COMMENT '0 for no restriction',
  `is_update_allowed` enum('allowed','not-allowed') DEFAULT NULL,
  `update_not_allowed_reason` varchar(200) DEFAULT NULL,
  `course_update_override_chapter` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
 */
	@Id
	@Column(name="student_course_purchase_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int student_course_purchase_id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="course_id")
	private int course_id;
	@Column(name="course_id")
	private DateTime purchase_date;
	  @Column(name="transaction_id")
		private String transaction_id;
	  @Column(name="purchase_course_version")
		private int purchase_course_version;
	  @Column(name="last_allowed_update_course_version")
		private int last_allowed_update_course_version;
	  
	  @Enumerated(EnumType.STRING)
		@Column(length = 8)
		private isUpdateAllowed is_update_allowed;
		  public enum isUpdateAllowed {
			  allowed,not_allowed
			}
	  
	@Column(name = "update_not_allowed_reason")
	private String update_not_allowed_reason;

	@Column(name = "course_update_override_chapter")
	private TinyIntTypeDescriptor course_update_override_chapter;

	@Column(name = "expiry_date")
	private DateTime expiry_date;

	@Column(name = "deleted")
	private TinyIntTypeDescriptor deleted;
}
