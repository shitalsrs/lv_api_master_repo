package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernetspring.model.Subject;

import com.hibernetspring.model.User;

public class SubjectDao {

	

	public SubjectDao() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean saveSubject(Subject subject)
	{
		Session sessionUser=HibernateUtil.getSessionFactory().openSession();
		boolean success = false;
		try{
			
			
			sessionUser.beginTransaction();
			sessionUser.save(subject);
			sessionUser.getTransaction().commit();
			
	            success = true;
			sessionUser.close();
			
		
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("subject not created"+e.getMessage());
			
		      success = false;
        }

        return success;
		
	}
	
	
	public List<Object> getAllsubjects()
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
	
			
			String hql = "FROM Subject s where s.subjectid>=:subjectid order by s.subjectid desc";
			
				
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
				query.setParameter("subjectid",getid);
			
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Object> veh=(ArrayList<Object>)results;	
			
		    
		    return veh;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
	
	public Subject getsubjects(int subid )
	{
		Subject subjects=new Subject();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
	
			
			String hql = "FROM Subject s where s.subjectid=:subjectid";
			
				
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
				query.setParameter("subjectid",subid);
			
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Subject> veh=(ArrayList<Subject>)results;	
		    Subject sub=veh.get(0);
			    return sub;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
	
	
	

	
	
}
