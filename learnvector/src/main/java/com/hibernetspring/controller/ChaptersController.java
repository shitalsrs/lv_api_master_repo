package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.Chapters;
import com.hibernetspring.service.ChapterService;


@RestController
@Path("chapter")
public class ChaptersController {

	@Autowired
	private ChapterService ChapterService;
	
	@POST
	@Path("/addchapter")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createChapter(@RequestBody Chapters chapter) {
		
		return ChapterService.saveChapter(chapter);
		
	
	}
}
