package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hibernetspring.model1.LiveSessions;
import com.hibernetspring.service.LiveSessionsService;




@RestController
@Path("livesession")
public class LiveSessionsController {

	@Autowired
	private LiveSessionsService liveSessionsService;
	
	@POST
	@Path("/addlivesession")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createLiveSession(@RequestBody LiveSessions liveSession) {
		
		return liveSessionsService.saveLiveSession(liveSession);
		
	
	}
	
}