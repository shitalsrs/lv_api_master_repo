package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.CourseChapMap;
import com.hibernetspring.repository.CourseChapMapRepo;


@Service
public class CourseChapMapService {

	@Autowired
	private CourseChapMapRepo courseChapMapRepo;
	
	public Response saveCourseMap(CourseChapMap courseChapMap) {
		
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(courseChapMapRepo.save(courseChapMap));		
			response.setSuccess(true);
			response.setMessage("Course chapter map record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create course chapter map record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
