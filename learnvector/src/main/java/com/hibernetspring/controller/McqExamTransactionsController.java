package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.McqExamTransactions;
import com.hibernetspring.service.McqExamTransactionService;

@RestController
@Path("mcqexamtransaction")
public class McqExamTransactionsController {
	@Autowired
	private McqExamTransactionService mcqExamTransactionService;
	@POST
	@Path("/addmcqtransaction")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMcqExam(@RequestBody McqExamTransactions mcqExamTransactions) {
		
		return mcqExamTransactionService.saveMcqTransaction(mcqExamTransactions);
		
	}
}
