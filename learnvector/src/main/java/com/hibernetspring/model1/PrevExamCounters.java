package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="prevexam_counters")
public class PrevExamCounters {

	/*
	 
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `exam_id_last_updated` datetime DEFAULT NULL,
  `exam_master_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)

	 */
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="registration_id")
	private int registration_id;
	@Column(name="exam_id")
	private int exam_id;
	@Column(name="exam_id_last_updated")
	private DateTime exam_id_last_updated;
	  @Column(name="exam_master_id")
		private int exam_master_id;
	  @Column(name="deleted")
		private TinyIntTypeDescriptor deleted;
	 
}
