package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hibernetspring.model1.LiveSessions;

public interface LiveSessionRepo extends JpaRepository<LiveSessions, Integer>{

}
