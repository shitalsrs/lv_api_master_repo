package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.McqBankTransactions;
import com.hibernetspring.service.McqBankService;

@RestController
@Path("mcqbank")
public class McqBankController {
	@Autowired
	private McqBankService mcqBankService;
	@POST
	@Path("/addmcqbank")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMcqBank(@RequestBody McqBankTransactions mcqBankTransactions) {
		
		return mcqBankService.saveMcqBank(mcqBankTransactions);
		
	}
}


