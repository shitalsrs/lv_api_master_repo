package com.hibernetspring.testapi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.dao.ChapterDAO;
import com.hibernetspring.dao.TopicDAO;
import com.hibernetspring.dao.TopicreportDAO;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model.Chapter;
import com.hibernetspring.model.Topic;
import com.hibernetspring.model.Topicreport;
@Path("topic")
public class TopicResource {

	@POST
	@Path("/addtopic")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createStock(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Topic topic = gson.fromJson(request,Topic.class);
			//System.out.println("gson data"+vehical.getV_carNo());
			
			TopicDAO topicDAO=new TopicDAO();
			topicDAO.saveTopic(topic);		
			response.setSuccess(true);
			response.setMessage("Topic  was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create topic, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@PUT
	@Path("/addtopicreport")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createTopicReport(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Topicreport topicReport = gson.fromJson(request,Topicreport.class);
			//System.out.println("gson data"+vehical.getV_carNo());
			
			TopicreportDAO topicreportDAO=new TopicreportDAO();
			topicreportDAO.saveTopicreport(topicReport);		
			response.setSuccess(true);
			response.setMessage("Topic report  was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create topic, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	@GET
	@Path("/gettopicreport/{userid}/{subid}/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopicReport(@PathParam("userid") String userid,@PathParam("subid") int subid,@PathParam("cid") int cid) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			//System.out.println("gson data"+vehical.getV_carNo());
			
			TopicreportDAO topicreportDAO=new TopicreportDAO();
				
			response.setSuccess(true);
			response.setMessage("Topic report  was created successfully.");
			
			response.setData(topicreportDAO.getAllreportTopic(userid,subid,cid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create topic, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
}
