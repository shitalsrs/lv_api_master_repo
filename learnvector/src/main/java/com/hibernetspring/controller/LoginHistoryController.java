package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.LoginHistory;
import com.hibernetspring.service.LoginHistoryService;

@RestController
@Path("loginhistory")
public class LoginHistoryController {
	@Autowired
	private LoginHistoryService loginHistoryService;
	
	@POST
	@Path("/addloginhistory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createHistory(@RequestBody LoginHistory loginHistory) {
		
		return loginHistoryService.saveLoginHistory(loginHistory);
		
	}
}