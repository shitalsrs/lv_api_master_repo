package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.McqBankTransactions;
import com.hibernetspring.repository.McqBankRepo;

@Service
public class McqBankService {

	@Autowired
	private McqBankRepo mcqBankRepo;
	
	public Response saveMcqBank(McqBankTransactions mcqBankTransactions) {
	
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(mcqBankRepo.save(mcqBankTransactions));		
			response.setSuccess(true);
			response.setMessage("Mcq bank transaction record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create mcq bank transaction record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
