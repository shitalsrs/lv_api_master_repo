package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.SubtopicMediaMapping;
import com.hibernetspring.service.SubtopicMediaMapService;


@RestController
@Path("subtopicmediamapping")
public class SubtopicMediaMapController {
	@Autowired
	private SubtopicMediaMapService subtopicMediaMapService;
	
	
	
	@POST
	@Path("/addsubtopicmediamap")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createSubtopicMediaMap(@RequestBody SubtopicMediaMapping subTopicMediaMapping) {
		
		return subtopicMediaMapService.saveSubtopicMediaMap(subTopicMediaMapping);
		
	}
}
