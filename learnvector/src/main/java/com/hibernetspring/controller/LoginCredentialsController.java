package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.LoginCredentials;
import com.hibernetspring.service.LoginCredentialService;


@RestController
@Path("logincredentials")
public class LoginCredentialsController {

	@Autowired
	private LoginCredentialService loginCredentialService;
	
	@POST
	@Path("/addlogincredential")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createLoginCredential(@RequestBody LoginCredentials loginCredentials) {
		
		return loginCredentialService.saveLoginHistory(loginCredentials);
		
	}
}
