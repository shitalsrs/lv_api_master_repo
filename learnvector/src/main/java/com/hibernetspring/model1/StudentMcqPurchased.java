package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;


@Entity
@Table(name ="student_mcq_purchased")
public class StudentMcqPurchased {

	
	/*
	  `mcq_purchase_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'per chapter',
  `student_id` int(11) DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `student_chapter_purchase_id` int(11) DEFAULT NULL COMMENT 'if any',
  `student_course_purchase_id` int(11) DEFAULT NULL COMMENT 'if any',
  `purchased_chapter_id` int(11) DEFAULT NULL COMMENT 'always',
  `purchased_course_id` int(11) DEFAULT NULL COMMENT 'if any depends on enum',
  `mcq_purchase_type` enum('bundled_with_chapter','bundled_with_course','only_mcq_course','only_mcq_chapter') DEFAULT NULL,
  `purchase_date` datetime DEFAULT NULL,
  `transaction_id` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`mcq_purchase_id`)

	 */
	@Id
	@Column(name="mcq_purchase_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int mcq_purchase_id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="registration_id")
	private int registration_id;
	@Column(name="student_chapter_purchase_id")
	private int student_chapter_purchase_id;
	@Column(name="student_course_purchase_id")
	private int student_course_purchase_id;
	@Column(name="purchased_chapter_id")
	private int purchased_chapter_id;
	@Column(name="purchased_course_id")
	private int purchased_course_id;
	
	  @Enumerated(EnumType.STRING)
		@Column(length =100)
		private mcqPurchase mcq_purchase_type;
		  public enum mcqPurchase {
			  bundled_with_chapter,bundled_with_course,only_mcq_course,only_mcq_chapter
			}
	  
	  @Column(name="purchase_date")
		private DateTime purchase_date;
	  
	  @Column(name="transaction_id")
		private String transaction_id;
	  @Column(name="deleted")
		private TinyIntTypeDescriptor deleted;
	public int getMcq_purchase_id() {
		return mcq_purchase_id;
	}
	public void setMcq_purchase_id(int mcq_purchase_id) {
		this.mcq_purchase_id = mcq_purchase_id;
	}
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	public int getRegistration_id() {
		return registration_id;
	}
	public void setRegistration_id(int registration_id) {
		this.registration_id = registration_id;
	}
	public int getStudent_chapter_purchase_id() {
		return student_chapter_purchase_id;
	}
	public void setStudent_chapter_purchase_id(int student_chapter_purchase_id) {
		this.student_chapter_purchase_id = student_chapter_purchase_id;
	}
	public int getStudent_course_purchase_id() {
		return student_course_purchase_id;
	}
	public void setStudent_course_purchase_id(int student_course_purchase_id) {
		this.student_course_purchase_id = student_course_purchase_id;
	}
	public int getPurchased_chapter_id() {
		return purchased_chapter_id;
	}
	public void setPurchased_chapter_id(int purchased_chapter_id) {
		this.purchased_chapter_id = purchased_chapter_id;
	}
	public int getPurchased_course_id() {
		return purchased_course_id;
	}
	public void setPurchased_course_id(int purchased_course_id) {
		this.purchased_course_id = purchased_course_id;
	}
	public mcqPurchase getMcq_purchase_type() {
		return mcq_purchase_type;
	}
	public void setMcq_purchase_type(mcqPurchase mcq_purchase_type) {
		this.mcq_purchase_type = mcq_purchase_type;
	}
	public DateTime getPurchase_date() {
		return purchase_date;
	}
	public void setPurchase_date(DateTime purchase_date) {
		this.purchase_date = purchase_date;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	  
	  
	  
	
}
