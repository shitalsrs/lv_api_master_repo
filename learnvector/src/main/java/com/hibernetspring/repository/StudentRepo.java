package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.Students;
public interface StudentRepo extends JpaRepository<Students,Integer> {

}
