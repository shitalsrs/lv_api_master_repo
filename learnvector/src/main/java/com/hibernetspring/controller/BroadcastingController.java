package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.Broadcasting;
import com.hibernetspring.service.BroadcastingService;



@RestController
@Path("broadcasting")
public class BroadcastingController {

	@Autowired
	private BroadcastingService broadcastingService;
	
	@POST
	@Path("/addbroadcasting")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createBroadcasting(@RequestBody Broadcasting broadcast) {
		
		return broadcastingService.saveBroadcast(broadcast);
		
	
	}
}