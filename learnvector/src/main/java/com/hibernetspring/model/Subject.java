package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="subject")
public class Subject {
	@Id
	@Column(name="subjectid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	int subjectid;
	@Column(name="subjectname")
	String subjectname;
	@Column(name="totalchapter")
	int totalchapter;
	@Column(name="totalvideo")
	int totalvideo;
	
	@Column(name="rating")
	int rating;
	
	@Column(name="teacher")
	String teacher;
	
	@Column(name="courseid")
	int courseid;
	
	@Column(name="subjettype")
	String subjettype;
	
	@Lob
	String description;
	
	@Column(name="imgur")
	String imgur;
	@Column(name="videourl")
	String videourl;
	
	@Column(name="updatedatetime")
	String updatedatetime;
	
	
	
	
	public int getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(int subjectid) {
		this.subjectid = subjectid;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	public int getTotalchapter() {
		return totalchapter;
	}
	public void setTotalchapter(int totalchapter) {
		this.totalchapter = totalchapter;
	}
	public int getTotalvideo() {
		return totalvideo;
	}
	public void setTotalvideo(int totalvideo) {
		this.totalvideo = totalvideo;
	}
	public String getUpdatedatetime() {
		return updatedatetime;
	}
	public void setUpdatedatetime(String updatedatetime) {
		this.updatedatetime = updatedatetime;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getImgur() {
		return imgur;
	}
	public void setImgur(String imgur) {
		this.imgur = imgur;
	}
	public String getVideourl() {
		return videourl;
	}
	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
