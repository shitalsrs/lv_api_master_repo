package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;


@Entity
@Table(name ="mcq_record")
public class McqRecord {

	/*
	 
	   `mcq_record_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mcq_id` int(11) DEFAULT NULL,
  `record_type` enum('question','answer','hint','choice') DEFAULT NULL,
  `text_media_id` int(11) DEFAULT NULL,
  `image_media_id` int(11) DEFAULT NULL,
  `video_media_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`mcq_record_id`)
	 */
	@Id
	@Column(name="mcq_record_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int mcq_record_id;
	@Column(name="mcq_id")
	private int mcq_id;
	 
	  
		@Enumerated(EnumType.STRING)
		@Column(length = 40)
		private recordType record_type;

		public enum recordType {
			question,answer,hint,choice
		}
	  
	  @Column(name="text_media_id")
		private int text_media_id;
	  @Column(name="image_media_id")
		private int image_media_id;
	  @Column(name="video_media_id")
		private int video_media_id;
	  @Column(name="deleted")
		private TinyIntTypeDescriptor deleted;
	public int getMcq_record_id() {
		return mcq_record_id;
	}
	public void setMcq_record_id(int mcq_record_id) {
		this.mcq_record_id = mcq_record_id;
	}
	public int getMcq_id() {
		return mcq_id;
	}
	public void setMcq_id(int mcq_id) {
		this.mcq_id = mcq_id;
	}
	public recordType getRecord_type() {
		return record_type;
	}
	public void setRecord_type(recordType record_type) {
		this.record_type = record_type;
	}
	public int getText_media_id() {
		return text_media_id;
	}
	public void setText_media_id(int text_media_id) {
		this.text_media_id = text_media_id;
	}
	public int getImage_media_id() {
		return image_media_id;
	}
	public void setImage_media_id(int image_media_id) {
		this.image_media_id = image_media_id;
	}
	public int getVideo_media_id() {
		return video_media_id;
	}
	public void setVideo_media_id(int video_media_id) {
		this.video_media_id = video_media_id;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	  
	  
	  
}
