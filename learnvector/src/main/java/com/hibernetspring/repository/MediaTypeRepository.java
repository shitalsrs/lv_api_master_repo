package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.hibernetspring.model1.MediaTypes;

public interface MediaTypeRepository extends JpaRepository<MediaTypes,Integer> {
	
	MediaTypes findByMedia_type_id(int id);

}
