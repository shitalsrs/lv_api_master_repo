package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.StudentMQChaptPurchased;
import com.hibernetspring.service.StudentMQChaptPurchasedService;



@RestController
@Path("studentmqchaptpurchased")
public class StudentMQChaptPurchasedController {
	@Autowired
	private StudentMQChaptPurchasedService studentMQChaptPurchasedService;
	
	//add StudChaptPurchased rest api
	
	@POST
	@Path("/addstudentmcqchapt")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createStudentMcqChapt(@RequestBody StudentMQChaptPurchased studentMQChaptPurchased) {
		
		return studentMQChaptPurchasedService.saveStudentMQChaptPurchased(studentMQChaptPurchased);
		
	}
}
	