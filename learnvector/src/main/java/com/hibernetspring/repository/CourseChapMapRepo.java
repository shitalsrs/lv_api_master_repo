package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.CourseChapMap;

public interface CourseChapMapRepo extends JpaRepository<CourseChapMap, Integer> {

}
