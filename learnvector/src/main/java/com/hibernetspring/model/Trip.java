package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="trip")
public class Trip {

	@Id
	@Column(name="t_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int t_id;
	
	@Column(name="t_name")
    private String t_name;

	@Column(name="t_quentity")
   private Float t_quentity;
	
	@Column(name="t_entrydate")
    private String t_entrydate;
	
	@Column(name="t_datetime")
	private String t_datetime;

	public int getT_id() {
		return t_id;
	}

	public void setT_id(int t_id) {
		this.t_id = t_id;
	}

	public String getT_name() {
		return t_name;
	}

	public void setT_name(String t_name) {
		this.t_name = t_name;
	}

	public Float getT_quentity() {
		return t_quentity;
	}

	public void setT_quentity(Float t_quentity) {
		this.t_quentity = t_quentity;
	}

	public String getT_datetime() {
		return t_datetime;
	}

	public void setT_datetime(String t_datetime) {
		this.t_datetime = t_datetime;
	}

	public String getT_entrydate() {
		return t_entrydate;
	}

	public void setT_entrydate(String t_entrydate) {
		this.t_entrydate = t_entrydate;
	}
		
}