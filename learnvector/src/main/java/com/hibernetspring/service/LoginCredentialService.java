package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.LoginCredentials;
import com.hibernetspring.repository.LoginCredentialRepo;

@Service
public class LoginCredentialService {


	@Autowired
	private LoginCredentialRepo loginCredentialRepo;
	
	public Response saveLoginHistory(LoginCredentials loginCredentials) {
	
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(loginCredentialRepo.save(loginCredentials));		
			response.setSuccess(true);
			response.setMessage("Login credential record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create login credential record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
