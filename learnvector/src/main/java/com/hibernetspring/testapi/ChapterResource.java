package com.hibernetspring.testapi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.dao.ChapterDAO;
import com.hibernetspring.dao.ChapterReportDAO;
import com.hibernetspring.dao.McqReportDAO;
import com.hibernetspring.dao.StockDAO;
import com.hibernetspring.dao.SubjectDao;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model.Chapter;
import com.hibernetspring.model.Chapterreport;
import com.hibernetspring.model.Stock;

@Path("chapter")
public class ChapterResource {

	@POST
	@Path("/addchapter")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createStock(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Chapter chapter = gson.fromJson(request,Chapter.class);
			//System.out.println("gson data"+vehical.getV_carNo());
			
			ChapterDAO chapterDAO=new ChapterDAO();
			chapterDAO.saveChapter(chapter);		
			response.setSuccess(true);
			response.setMessage("stock  was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create stock, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/chapterlist/{subjectid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubjectDetailList(@PathParam("subjectid") int subjectid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		ChapterDAO chapterDao=new ChapterDAO();
		
			response.setSuccess(true);
			response.setMessage("Chapter List");
			
			response.setData(chapterDao.getAllChapter(subjectid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
	
	@GET
	@Path("/getchapterids/{subjectid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChapterids(@PathParam("subjectid") int subjectid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		ChapterDAO chapterDao=new ChapterDAO();
		
			response.setSuccess(true);
			response.setMessage("Chapter List");
			
			response.setData(chapterDao.getAllChapter(subjectid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/chapterlistdetail/{subjectid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubjectDetail(@PathParam("subjectid") int subjectid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		ChapterDAO chapterDao=new ChapterDAO();
		
			response.setSuccess(true);
			response.setMessage("Chapter List");
			
			/*
			 * EdzaniaChapterRepository edzaniaChapterRepository=new
			 * EdzaniaChapterRepository(); StudChapter studChapter = new StudChapter();
			 * studChapter = edzaniaChapterRepository.getChapterdata(chapterId);
			 * EdzaniaMcqRepository edzaniaMcqRepository = new EdzaniaMcqRepository();
			 * McqResponse mcqResponse=new McqResponse();
			 * mcqResponse.setStudMcq(edzaniaMcqRepository.getAllMcqs(chapterId));
			 * 
			 * EdzaniaVideoRepository edzaniaVideoRepository = new EdzaniaVideoRepository();
			 * VideoResponse videoResponse=new VideoResponse();
			 * videoResponse.setStudVideo(edzaniaVideoRepository.getChapterVideos(chapterId)
			 * ); //studChapter.setMcqids((ArrayList<StudMcq>) mcqResponse.getStudMcq());
			 * 
			 * studChapter.setVideodata(videoResponse.getStudVideo()); Chapter chapter=new
			 * Chapter(); chapter=(Chapter) chapterDao.getAllChapterDetail(subjectid);
			 */
			
			response.setData(chapterDao.getAllChapterDetail(subjectid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
	
	//return Response.ok().status(200).entity("I am good").build();
	
   }
	
	
	@GET
	@Path("/chapterdetail/{chapterid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChapterDetail(@PathParam("chapterid") int chapterid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		ChapterDAO chapterDao=new ChapterDAO();
		
			response.setSuccess(true);
			response.setMessage("Chapter List");
			
		
			
			response.setData(chapterDao.getChapterDetail(chapterid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
	
	//return Response.ok().status(200).entity("I am good").build();
	
   }
		
	
	@PUT
	@Path("/addchaptrreport")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createChaptReport(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Chapterreport chapterrepo = gson.fromJson(request,Chapterreport.class);
			//System.out.println("gson data"+vehical.getV_carNo());
			
			ChapterReportDAO chapterRepoDAO=new ChapterReportDAO();
			chapterRepoDAO.saveChapterReport(chapterrepo);		
			response.setSuccess(true);
			response.setMessage("Chapter report  was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create chapter report, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@POST
	@Path("/updatechaptrreport/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createChaptReport(@PathParam("cid") int cid,String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Chapterreport chapterrepo = gson.fromJson(request,Chapterreport.class);
			//System.out.println("gson data"+vehical.getV_carNo());
			
			ChapterReportDAO chapterRepoDAO=new ChapterReportDAO();
			chapterRepoDAO.updateChapterReport(chapterrepo,cid);		
			response.setSuccess(true);
			response.setMessage("Chapter report  was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create chapter report, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	@GET
	@Path("/getchaptrreport/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createChaptReport(@PathParam("cid") int cid) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			
			//System.out.println("gson data"+vehical.getV_carNo());
			
			ChapterReportDAO chapterRepoDAO=new ChapterReportDAO();
			//chapterRepoDAO.updateChapterReport(chapterrepo,cid);		
			
			response.setSuccess(true);
			response.setMessage("Chapter report  was created successfully.");
			response.setData(chapterRepoDAO.getchapter(cid)	);
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create chapter report, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	@GET
	@Path("/chaptrreport/{userid}/{subid}/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentChapterReport(@PathParam("userid") String userid,@PathParam("subid") int subid,@PathParam("cid") int cid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			ChapterReportDAO chapterReportDAO=new ChapterReportDAO();
		
			response.setSuccess(true);
			response.setMessage("Chapter Report List");
			
			response.setData(chapterReportDAO.getAllreportSubject(userid,subid,cid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/listchaptrreport/{userid}/{subid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentSubject(@PathParam("userid") String userid,@PathParam("subid") int subid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			ChapterReportDAO chapterReportDAO=new ChapterReportDAO();
		
			response.setSuccess(true);
			response.setMessage("Chapter Report List");
			
			response.setData(chapterReportDAO.getAllreportChapt(userid,subid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
	
}
