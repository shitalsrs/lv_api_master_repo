package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="mcqbank_transactions")
public class McqBankTransactions {
/*
 
   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `registration_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `mcq_bank_id` int(11) DEFAULT NULL,
  `mcq_id` int(11) DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `answered` tinyint(1) DEFAULT NULL,
  `completed_on` datetime DEFAULT NULL,
  `expired` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `answer_option_selected` int(11) DEFAULT NULL,
  `marks` int(11) DEFAULT NULL,
 */
	@Id
	@Column(name="mcq_batch_size")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="registration_id")
	private int registration_id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="mcq_bank_id")
	private int mcq_bank_id;
	@Column(name="mcq_id")
	private int mcq_id;
	@Column(name="chapter_id")
	private int chapter_id;
	@Column(name="created_on")
	private int created_on;
	@Column(name="answered")
	private TinyIntTypeDescriptor answered;
		@Column(name="completed_on")
		private DateTime completed_on;
	 @Column(name="expired")
		private TinyIntTypeDescriptor expired;
	 @Column(name="deleted")
		private TinyIntTypeDescriptor deleted;
	 
	 @Column(name="mcq_batch_size")
		private int answer_option_selected;
	 @Column(name="mcq_batch_size")
		private int  marks;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRegistration_id() {
		return registration_id;
	}
	public void setRegistration_id(int registration_id) {
		this.registration_id = registration_id;
	}
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	public int getMcq_bank_id() {
		return mcq_bank_id;
	}
	public void setMcq_bank_id(int mcq_bank_id) {
		this.mcq_bank_id = mcq_bank_id;
	}
	public int getMcq_id() {
		return mcq_id;
	}
	public void setMcq_id(int mcq_id) {
		this.mcq_id = mcq_id;
	}
	public int getChapter_id() {
		return chapter_id;
	}
	public void setChapter_id(int chapter_id) {
		this.chapter_id = chapter_id;
	}
	public int getCreated_on() {
		return created_on;
	}
	public void setCreated_on(int created_on) {
		this.created_on = created_on;
	}
	public TinyIntTypeDescriptor getAnswered() {
		return answered;
	}
	public void setAnswered(TinyIntTypeDescriptor answered) {
		this.answered = answered;
	}
	public DateTime getCompleted_on() {
		return completed_on;
	}
	public void setCompleted_on(DateTime completed_on) {
		this.completed_on = completed_on;
	}
	public TinyIntTypeDescriptor getExpired() {
		return expired;
	}
	public void setExpired(TinyIntTypeDescriptor expired) {
		this.expired = expired;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	public int getAnswer_option_selected() {
		return answer_option_selected;
	}
	public void setAnswer_option_selected(int answer_option_selected) {
		this.answer_option_selected = answer_option_selected;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
}
