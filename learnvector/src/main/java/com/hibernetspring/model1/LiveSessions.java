package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="live_session")
public class LiveSessions {

	/*
	 `live_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `live_name` varchar(250) DEFAULT NULL,
  `intro_image` int(11) DEFAULT NULL,
  `intro_text` int(11) DEFAULT NULL,
  `intro_video` int(11) DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `live_type` enum('chapter_series','course_series','promotional','demo','bundle_series') DEFAULT NULL COMMENT 'needs to be refined',
  `live_date` datetime DEFAULT NULL,
  PRIMARY KEY (`live_id`)
	 */
	
	@Id
	@Column(name="course_chapter_mapping_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int live_id;
	@Column(name="live_name")
	private String live_name;
	@Column(name="intro_image")
	private int intro_image;
	@Column(name="intro_text")
	private int intro_text;
	@Column(name="intro_video")
	private int intro_video;
	@Column(name="chapter_id")
	private int chapter_id;
	@Column(name="course_id")
	private int course_id;
	
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;


	@Enumerated(EnumType.STRING)
	@Column(length = 8)
	private liveType live_type;
	  public enum liveType {
		  chapter_series,course_series,promotional,demo,bundle_series
		}
	  
	  	  @Column(name="live_date")
		private DateTime live_date;

		public int getLive_id() {
			return live_id;
		}

		public void setLive_id(int live_id) {
			this.live_id = live_id;
		}

		public String getLive_name() {
			return live_name;
		}

		public void setLive_name(String live_name) {
			this.live_name = live_name;
		}

		public int getIntro_image() {
			return intro_image;
		}

		public void setIntro_image(int intro_image) {
			this.intro_image = intro_image;
		}

		public int getIntro_text() {
			return intro_text;
		}

		public void setIntro_text(int intro_text) {
			this.intro_text = intro_text;
		}

		public int getIntro_video() {
			return intro_video;
		}

		public void setIntro_video(int intro_video) {
			this.intro_video = intro_video;
		}

		public int getChapter_id() {
			return chapter_id;
		}

		public void setChapter_id(int chapter_id) {
			this.chapter_id = chapter_id;
		}

		public int getCourse_id() {
			return course_id;
		}

		public void setCourse_id(int course_id) {
			this.course_id = course_id;
		}

		public TinyIntTypeDescriptor getDeleted() {
			return deleted;
		}

		public void setDeleted(TinyIntTypeDescriptor deleted) {
			this.deleted = deleted;
		}

		public liveType getLive_type() {
			return live_type;
		}

		public void setLive_type(liveType live_type) {
			this.live_type = live_type;
		}

		public DateTime getLive_date() {
			return live_date;
		}

		public void setLive_date(DateTime live_date) {
			this.live_date = live_date;
		}
	 
	  	  
	  	  
	  	  
	  	  
	  	  
}
