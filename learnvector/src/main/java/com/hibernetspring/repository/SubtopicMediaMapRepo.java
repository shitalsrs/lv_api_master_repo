package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.SubtopicMediaMapping;

public interface SubtopicMediaMapRepo extends JpaRepository<SubtopicMediaMapping, Integer>{

}
