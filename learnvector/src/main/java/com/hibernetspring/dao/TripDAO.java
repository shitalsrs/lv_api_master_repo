package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernetspring.model.Trip;
import com.hibernetspring.dao.StockDAO;

public class TripDAO {

	//SessionFactory sessionFactoryTrip=null;
	//Session sessionTrip=null;
	
	public  TripDAO(){
		//sessionFactoryTrip = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    	//System.out.println("Sessionfactory"+sessionFactoryTrip);
	}
	
	public Trip saveTrip(Trip trip)
	{
		try{
			//System.out.println("vehicalno"+vehical.getV_lat()+"");
			//System.out.println("vehical"+vehical.getV_driverLicenNo());
			//sessionFactoryTrip = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		    
			//sessionTrip=this.sessionFactoryTrip.openSession();
			Session sessionTrip=HibernateUtil.getSessionFactory().openSession();
			sessionTrip.beginTransaction();
			sessionTrip.save(trip);
			sessionTrip.getTransaction().commit();
			  //update stock on jetty
			
			StockDAO stockdao=new StockDAO();
			stockdao.updateStockDaoJetty(trip.getT_quentity(),trip.getT_entrydate(),trip.getT_datetime());
			
			//sessionTrip.close();
			//sessionFactoryTrip.close();
			
			
			
	
		}catch(Exception e){
			e.printStackTrace();
			
			System.out.println("Session error"+e.getMessage());
			//sessionTrip.getTransaction().rollback();
		}
		return trip;
	}
	
	
	public List<Trip> getTripList(String dates)
	{
		List<Trip> trp=new ArrayList<Trip>();
		Session sessionTrip=HibernateUtil.getSessionFactory().openSession();
		try{
         /*String fdate="11";
         String ddate="13";*/
         
         
        /* DateFormat df = new SimpleDateFormat("yyyy/mm/dd");
         Date frmDate=df.parse("2014/01/01");
         Date toDate=df.parse("2014/09/01");
			*/
      /*   DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
         Date date = new Date();
         System.out.println(dateFormat.format(date));*/
			//sessionFactoryTrip = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		    
		
			//sessionTrip = this.sessionFactoryTrip.openSession();
		
		
			//String hql = "FROM Vehical AS Vehical";
			String hql = "FROM Trip ts where ts.t_entrydate=:t_entrydate";
		
			//String hql = "FROM Trip t where t.td_driverid=:t_driverid and t.t_picdatetime=:fromDate";
			
			//String hql = "FROM Trip t where t.td_driverid=:t_driverid and t.t_picdatetime between :fromDate and :toDate";
			
	Query query = sessionTrip.createQuery(hql);
			query.setParameter("t_entrydate",dates);
			  System.out.println(dates+"");
			
		/*query.setParameter("fromDate",fdate);
			query.setParameter("toDate",ddate);*/
			List results = ((org.hibernate.Query) query).list();
		
		    List<Trip> trips=(ArrayList<Trip>)results;	
		     System.out.println(trips.get(0).toString());
		    return trips;
			
		    
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionTrip.close();
			//sessionFactoryTrip.close();
		}
		return trp;
	}
	
	
}
