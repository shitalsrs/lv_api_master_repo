package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;

@Entity
@Table(name ="media_types")
public class MediaTypes {

	@Id
	@Column(name="media_type_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int media_type_id;
	@Column(name="media_type_name")
	private String media_type_name;
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	
	public int getMedia_type_id() {
		return media_type_id;
	}
	public void setMedia_type_id(int media_type_id) {
		this.media_type_id = media_type_id;
	}
	public String getMedia_type_name() {
		return media_type_name;
	}
	public void setMedia_type_name(String media_type_name) {
		this.media_type_name = media_type_name;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
}
