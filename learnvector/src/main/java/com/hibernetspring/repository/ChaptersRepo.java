package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.Chapters;

public interface ChaptersRepo extends JpaRepository<Chapters, Integer> {

}
