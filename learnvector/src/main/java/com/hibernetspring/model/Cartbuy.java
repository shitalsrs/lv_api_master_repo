package com.hibernetspring.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name ="cartbuy")
public class Cartbuy {
	
	@Id
	@Column(name="cbid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int cbid;
	
	@Column(name="u_name")
     String u_name;
	
	@Column(name="subjectid")
	int subjectid;
	@Column(name="subjectname")
	String subjectname;
	
	@Column(unique = true)
	String subjectuser;
	

	@Column(name="chapterlist")
	 String chapterlist;
	
	@Column(name="incart")
	 Boolean incart;
	
	
/*@ElementCollection
	//private Collection<Chapterlist> lisOfAddresses = new ArrayList<Chapterlist>();
	
	
	
	
	public Collection<Chapterlist> getLisOfAddresses() {
		return lisOfAddresses;
	}

	public void setLisOfAddresses(Collection<Chapterlist> lisOfAddresses) {
		this.lisOfAddresses = lisOfAddresses;
	}
*/
	

	

	@Column(name="price")
	String price;
	




	public String getChapterlist() {
		return chapterlist;
	}

	public void setChapterlist(String chapterlist) {
		this.chapterlist = chapterlist;
	}

	@Column(name="cartadddatetime")
	String cartadddatetime;
	
	@Column(name="validdatetime")
	String validdatetime;
	
	@Column(name="buydatetime")
	String buydatetime;
	
	public int getCbid() {
		return cbid;
	}

	public void setCbid(int cbid) {
		this.cbid = cbid;
	}

	public int getSubjectid() {
		return subjectid;
	}

	public void setSubjectid(int subjectid) {
		this.subjectid = subjectid;
	}

	public String getSubjectname() {
		return subjectname;
	}

	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}








	public Boolean getIncart() {
		return incart;
	}

	public void setIncart(Boolean incart) {
		this.incart = incart;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCartadddatetime() {
		return cartadddatetime;
	}

	public void setCartadddatetime(String cartadddatetime) {
		this.cartadddatetime = cartadddatetime;
	}

	public String getValiddatetime() {
		return validdatetime;
	}

	public void setValiddatetime(String validdatetime) {
		this.validdatetime = validdatetime;
	}

	public String getBuydatetime() {
		return buydatetime;
	}

	public void setBuydatetime(String buydatetime) {
		this.buydatetime = buydatetime;
	}

	public String getU_name() {
		return u_name;
	}

	public void setU_name(String u_name) {
		this.u_name = u_name;
	}


	
	
	
	

}
