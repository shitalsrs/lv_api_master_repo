package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;

@Entity
@Table(name ="chapter_updates")
public class ChapterUpdatesPerReg {
/*
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `registration_id` int(11) DEFAULT NULL,
  `update_type` enum('add','delete','modify') DEFAULT NULL,
  `update_candidate_name` enum('chapter','mcq_chapter','course') DEFAULT NULL COMMENT 'if chapter then refer to chapter_purchased, if mcq_then check mcq_purchased',
  `candidate_id` int(11) DEFAULT NULL COMMENT 'mcq_set_id, chapter id',
  `processed` tinyint(1) DEFAULT NULL COMMENT 'yes, no',
  `process_datetime` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
 */
		
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="subject_id")
	private int registration_id;
	  
	
	@Enumerated(EnumType.STRING)
    @Column(length = 8)
    private updateType update_type;
	  public enum updateType {
		  add,delete,modify
		}
	  @Enumerated(EnumType.STRING)
	    @Column(length = 8)
	    private updateCandidate update_candidate_name;
		  public enum updateCandidate {
			  chapter,mcq_chapter,course
			}
	
	  
	  @Column(name="subject_id")
		private int candidate_id;
	  
	  @Column(name="processed")
		private TinyIntTypeDescriptor processed;
	  @Column(name="subject_id")
		private DateTime process_datetime;
	  
		@Column(name="deleted")
		private TinyIntTypeDescriptor deleted;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getRegistration_id() {
			return registration_id;
		}

		public void setRegistration_id(int registration_id) {
			this.registration_id = registration_id;
		}

		public updateType getUpdate_type() {
			return update_type;
		}

		public void setUpdate_type(updateType update_type) {
			this.update_type = update_type;
		}

		public updateCandidate getUpdate_candidate_name() {
			return update_candidate_name;
		}

		public void setUpdate_candidate_name(updateCandidate update_candidate_name) {
			this.update_candidate_name = update_candidate_name;
		}

		public int getCandidate_id() {
			return candidate_id;
		}

		public void setCandidate_id(int candidate_id) {
			this.candidate_id = candidate_id;
		}

		public TinyIntTypeDescriptor getProcessed() {
			return processed;
		}

		public void setProcessed(TinyIntTypeDescriptor processed) {
			this.processed = processed;
		}

		public DateTime getProcess_datetime() {
			return process_datetime;
		}

		public void setProcess_datetime(DateTime process_datetime) {
			this.process_datetime = process_datetime;
		}

		public TinyIntTypeDescriptor getDeleted() {
			return deleted;
		}

		public void setDeleted(TinyIntTypeDescriptor deleted) {
			this.deleted = deleted;
		}
		
		
}
