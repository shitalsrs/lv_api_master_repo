package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.hibernetspring.model.Chapterreport;
import com.hibernetspring.model.Stock;



public class ChapterReportDAO {

	public Chapterreport saveChapterReport(Chapterreport chapterrepo)
	{
		
		//SessionFactory sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		//Session sessionChap=sessionFactoryStock.openSession();
	
		Session sessionChap=HibernateUtil.getSessionFactory().openSession();
		
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=sessionFactoryStock.openSession();
		
			
			
			sessionChap.beginTransaction();
			sessionChap.save(chapterrepo);
			sessionChap.getTransaction().commit();
			return chapterrepo;
			//sessionFactoryStock.close();
			
	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Session error"+e.getMessage());
			//sessionStock.getTransaction().rollback();
		}finally{
			sessionChap.close();
			//sessionFactoryVehical.close();
		}
		return chapterrepo;
	}
	
	
	public Chapterreport updateChapterReport(Chapterreport chapterrepo,int cid)
	{
		
		//SessionFactory sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		//Session sessionChap=sessionFactoryStock.openSession();
	
		Session sessionChap=HibernateUtil.getSessionFactory().openSession();
		
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=sessionFactoryStock.openSession();
			Chapterreport creport=getchapter(cid);
			creport.setTotalmcqattend(chapterrepo.getTotalmcqattend());
		

			sessionChap.beginTransaction();
			//sessionChap.save(creport);
			
	    	sessionChap.update(creport);
			sessionChap.getTransaction().commit();
			return chapterrepo;
			//sessionFactoryStock.close();
			
	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Session error"+e.getMessage());
			//sessionStock.getTransaction().rollback();
		}finally{
			sessionChap.close();
			//sessionFactoryVehical.close();
		}
		return chapterrepo;
	}
	
	public List<Object> getAllreportChapt(String userid,int subid)
	{
		List<Object> subjects=new ArrayList<Object>();
	    Transaction tx = null;
	    Session sessionChaptrepo=HibernateUtil.getSessionFactory().openSession();
	    
		try{			
			String hql = "FROM Chapterreport cr where cr.userid=:userid and cr.subid=:subid";
			 //int getid=1;
				Query query = sessionChaptrepo.createQuery(hql);
				query.setParameter("userid",userid);
				query.setParameter("subid",subid);
				//query.setParameter("cid",chapterid);
				
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Object> veh=(ArrayList<Object>)results;	
			
		    
		    return veh;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionChaptrepo.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
	
	public List<Object> getAllreportSubject(String userid,int subid,int chapterid)
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionChaptrepo=HibernateUtil.getSessionFactory().openSession();
		
		try{
			
			
			String hql = "FROM Chapterreport cr where cr.userid=:userid and cr.subid=:subid and cr.cid=:cid";
			
		
				Query query = sessionChaptrepo.createQuery(hql);
				query.setParameter("userid",userid);
			    query.setParameter("subid",subid);
				query.setParameter("cid",chapterid);
				
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Object> veh=(ArrayList<Object>)results;	
			
		    
		    return veh;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionChaptrepo.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
	
	public Chapterreport getchapter(int cid)
	{
		
		
		Chapterreport getchap=new Chapterreport();
		Session sessionChapt=HibernateUtil.getSessionFactory().openSession();
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock = this.sessionFactoryStock.openSession();
			
			//String hql = "FROM Vehical AS Vehical";
			String hql = "FROM Chapterreport cr where cr.cid=:cid";
     
			
		 int dates=1;
			Query query = sessionChapt.createQuery(hql);
			query.setParameter("cid",cid);
			  System.out.println(dates+"");

			List results = ((org.hibernate.Query) query).list();
		    List<Chapterreport> chaptr=(ArrayList<Chapterreport>)results;	
		    System.out.println(chaptr.get(0).toString());
		    Chapterreport s=chaptr.get(0);
		     getchap =(Chapterreport)sessionChapt.get(Chapterreport.class,s.getCrid()); 
    
		    return getchap;
			
		    
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionChapt.close();
			//sessionFactoryStock.close();
		}
		return getchap;
	}
}
