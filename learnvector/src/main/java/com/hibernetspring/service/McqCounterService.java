package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.McqCounters;
import com.hibernetspring.repository.McqCounterRepo;

@Service
public class McqCounterService {
	@Autowired
	private McqCounterRepo mcqCounterRepo;
	
	public Response saveMcqCounter(McqCounters mcqCounter) {
		
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();	
		try {		
			response.setData(mcqCounterRepo.save(mcqCounter));		
			response.setSuccess(true);
			response.setMessage("Mcq counter record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create mcq counter record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
