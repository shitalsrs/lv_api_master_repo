package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="progresscard")
public class Progresscard {
	
	@Id
	@Column(name="userid")
	@GeneratedValue(strategy =GenerationType.AUTO)
	int userid;
	@Column(name="subjectid")
	String subjectid;
	@Column(name="subjectscore")
	String subjectscore;
	@Column(name="chapterid")
	String chapterid;
	@Column(name="scoremcq")
	String scoremcq;
	@Column(name="scorevideo")
	String scorevideo;
	@Column(name="lastseenchpaterid")
	String lastseenchpaterid;
	@Column(name="updatedatetime")
	String updatedatetime;
	
	
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(String subjectid) {
		this.subjectid = subjectid;
	}
	public String getSubjectscore() {
		return subjectscore;
	}
	public void setSubjectscore(String subjectscore) {
		this.subjectscore = subjectscore;
	}
	public String getChapterid() {
		return chapterid;
	}
	public void setChapterid(String chapterid) {
		this.chapterid = chapterid;
	}
	public String getScoremcq() {
		return scoremcq;
	}
	public void setScoremcq(String scoremcq) {
		this.scoremcq = scoremcq;
	}
	public String getScorevideo() {
		return scorevideo;
	}
	public void setScorevideo(String scorevideo) {
		this.scorevideo = scorevideo;
	}
	public String getLastseenchpaterid() {
		return lastseenchpaterid;
	}
	public void setLastseenchpaterid(String lastseenchpaterid) {
		this.lastseenchpaterid = lastseenchpaterid;
	}
	public String getUpdatedatetime() {
		return updatedatetime;
	}
	public void setUpdatedatetime(String updatedatetime) {
		this.updatedatetime = updatedatetime;
	}

	
}
