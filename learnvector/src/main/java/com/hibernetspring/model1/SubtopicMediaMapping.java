package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="subtopic_media_mapping")
public class SubtopicMediaMapping {
/*
 `subtopic_media_mapping_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subtopic_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `intro_text` varchar(450) DEFAULT NULL,
  `media_type_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
 */
	
	@Id
	@Column(name="subtopic_media_mapping_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int subtopic_media_mapping_id;
	@Column(name="subtopic_id")
	private int subtopic_id;
	@Column(name="media_id")
	private int media_id;
	@Column(name="intro_text")
	private String intro_text;
	@Column(name="media_type_id")
	private int media_type_id;
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	
	
	public int getSubtopic_media_mapping_id() {
		return subtopic_media_mapping_id;
	}
	public void setSubtopic_media_mapping_id(int subtopic_media_mapping_id) {
		this.subtopic_media_mapping_id = subtopic_media_mapping_id;
	}
	public int getSubtopic_id() {
		return subtopic_id;
	}
	public void setSubtopic_id(int subtopic_id) {
		this.subtopic_id = subtopic_id;
	}
	public int getMedia_id() {
		return media_id;
	}
	public void setMedia_id(int media_id) {
		this.media_id = media_id;
	}
	public String getIntro_text() {
		return intro_text;
	}
	public void setIntro_text(String intro_text) {
		this.intro_text = intro_text;
	}
	public int getMedia_type_id() {
		return media_type_id;
	}
	public void setMedia_type_id(int media_type_id) {
		this.media_type_id = media_type_id;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
	
	
}
