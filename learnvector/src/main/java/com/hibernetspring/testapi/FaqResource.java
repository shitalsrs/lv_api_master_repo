package com.hibernetspring.testapi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.dao.FaqDAO;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model.Faq;
@Path("/faq")
public class FaqResource {

	@POST
	@Path("/addfaq")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createfaq(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Faq faq = gson.fromJson(request,Faq.class);
			
			FaqDAO faqDao=new FaqDAO();
			Boolean b=faqDao.savefaq(faq);	
			if(b) {
			response.setSuccess(true);
			response.setMessage("faq created created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
			}else {
				response.setSuccess(false);
				response.setMessage("faq not created successfully.");
				return Response.ok().status(200).entity(gson.toJson(response)).build();
			}
		}catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create faq, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubjectDetailList(@PathParam("cid") String cid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		FaqDAO faqDao=new FaqDAO();
		
			response.setSuccess(true);
			response.setMessage("faq List");
			
			response.setData(faqDao.getAllfaq(cid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  faq list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
}
