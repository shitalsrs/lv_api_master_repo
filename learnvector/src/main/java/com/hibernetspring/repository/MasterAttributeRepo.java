package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hibernetspring.model1.MasterAttributes;

public interface MasterAttributeRepo extends JpaRepository<MasterAttributes,Integer>{

}
 