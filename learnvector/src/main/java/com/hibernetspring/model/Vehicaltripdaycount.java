package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vehicaltripdaycount {

	@Id
	private int vt_id;
	
	
    private String vt_entrydate;
	
	
	   private Double vt_volume;
	
	   @Column(name="vt_id")
	public int getVt_id() {
		return vt_id;
	}

	public void setVt_id(int vt_id) {
		this.vt_id = vt_id;
	}
	@Column(name = "vt_entrydate")
	public String getVt_entrydate() {
		return vt_entrydate;
	}

	public void setVt_entrydate(String vt_entrydate) {
		this.vt_entrydate = vt_entrydate;
	}
	@Column(name = "vt_volume")
	public Double getVt_volume() {
		return vt_volume;
	}

	public void setVt_volume(Double vt_volume) {
		this.vt_volume = vt_volume;
	}
	
	
	
	
	
}
