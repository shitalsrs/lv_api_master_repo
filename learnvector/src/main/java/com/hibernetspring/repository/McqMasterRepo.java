package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hibernetspring.model1.McqMaster;


public interface McqMasterRepo extends JpaRepository<McqMaster,Integer> {

}
