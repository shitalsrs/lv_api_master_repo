package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.ChapterUpdatesPerReg;
import com.hibernetspring.service.ChapterUpdateService;

@RestController
@Path("chapterupdate")
public class ChapterUpdatesController {

	@Autowired
	private ChapterUpdateService ChapterUpdateService;
	
	@POST
	@Path("/addchapterupdate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createChapterUpdate(@RequestBody ChapterUpdatesPerReg chapterUpdatePer) {
		
		return ChapterUpdateService.saveChapterUpdate(chapterUpdatePer);
		
	
	}
}