package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.LiveSessions;
import com.hibernetspring.repository.LiveSessionRepo;


@Service
public class LiveSessionsService {

	@Autowired
	private LiveSessionRepo LiveSessionRepo;
	
	public Response saveLiveSession(LiveSessions liveSession) {
		
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(LiveSessionRepo.save(liveSession));		
			response.setSuccess(true);
			response.setMessage("Live session record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create live session record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
