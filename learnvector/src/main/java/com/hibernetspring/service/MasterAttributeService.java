package com.hibernetspring.service;

import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.MasterAttributes;
import com.hibernetspring.repository.MasterAttributeRepo;

@Service
public class MasterAttributeService {
	@Autowired
	private MasterAttributeRepo masterAttributeRepo;
	
	public Response saveMasterAttribute(MasterAttributes masterAttributes) {
	
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(masterAttributeRepo.save(masterAttributes));		
			response.setSuccess(true);
			response.setMessage("Master attribute record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create master attribute record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
