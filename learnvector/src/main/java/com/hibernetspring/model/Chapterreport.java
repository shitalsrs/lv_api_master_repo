package com.hibernetspring.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name ="chapterreport")
public class Chapterreport{


	@Id
	@Column(name="crid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int crid;
	
	@Column(name="cid",unique = true)
	int cid;
	
	@Column(name="cname")
	String cname;
	
	@Column(name="subid")
	int subid;
	
	@Column(name="subname")
	String subname;
	
	@Column(name="userid")
	String userid;
	@Column(name="totalmcq")
	int totalmcq;
	@Column(name="totalmcqattend")
	int totalmcqattend;
	
	@Column(name="totalvideo")
	int totalvideo;
	
	@Column(name="totalvideoattend")
	int totalvideoattend;
	
	@Column(name="updatedatetime")
	String updatedatetime;
	
	

	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public int getTotalmcqattend() {
		return totalmcqattend;
	}
	public void setTotalmcqattend(int totalmcqattend) {
		this.totalmcqattend = totalmcqattend;
	}
	public int getTotalvideoattend() {
		return totalvideoattend;
	}
	public void setTotalvideoattend(int totalvideoattend) {
		this.totalvideoattend = totalvideoattend;
	}
	public String getUpdatedatetime() {
		return updatedatetime;
	}
	public void setUpdatedatetime(String updatedatetime) {
		this.updatedatetime = updatedatetime;
	}
	public int getCrid() {
		return crid;
	}
	public void setCrid(int crid) {
		this.crid = crid;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public int getSubid() {
		return subid;
	}
	public void setSubid(int subid) {
		this.subid = subid;
	}
	public String getSubname() {
		return subname;
	}
	public void setSubname(String subname) {
		this.subname = subname;
	}
	public int getTotalmcq() {
		return totalmcq;
	}
	public void setTotalmcq(int totalmcq) {
		this.totalmcq = totalmcq;
	}
	public int getTotalvideo() {
		return totalvideo;
	}
	public void setTotalvideo(int totalvideo) {
		this.totalvideo = totalvideo;
	}
	
	

   // private List<StudMcq> mcqdata;
	
	/* @Column(name="topiclist") private List<Topic> topicdata; */
	 
	
	
	
}
