package com.hibernetspring.testapi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.dao.ChapterDAO;
import com.hibernetspring.dao.McqDAO;
import com.hibernetspring.dao.McqReportDAO;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model.Mcq;
import com.hibernetspring.model.Mcqreport;

@Path("/mcq")
public class McqResource {

	@POST
	@Path("/addmcq")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createmcq(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Mcq mcq = gson.fromJson(request,Mcq.class);
			
			McqDAO mcqDao=new McqDAO();
			Boolean b=mcqDao.saveMcq(mcq);	
			if(b) {
			response.setSuccess(true);
			response.setMessage("Mcq created created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
			}else {
				response.setSuccess(false);
				response.setMessage("Mcq not created successfully.");
				return Response.ok().status(200).entity(gson.toJson(response)).build();
			}
		}catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create Mcq, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubjectDetailList(@PathParam("cid") String cid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		McqDAO mcqDao=new McqDAO();
		
			response.setSuccess(true);
			response.setMessage("Mcq List");
			
			response.setData(mcqDao.getAllmcq(cid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
	
	
	
	@POST
	@Path("/addmcqreport")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createmcqreport(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Mcqreport mcqreport = gson.fromJson(request,Mcqreport.class);
			
			McqReportDAO mcqreportDao=new McqReportDAO();
			Boolean b=mcqreportDao.saveMcqReport(mcqreport);	
			if(b) {
			response.setSuccess(true);
			response.setMessage("McqReport created created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
			}else {
				response.setSuccess(false);
				response.setMessage("Mcq not created successfully.");
				return Response.ok().status(200).entity(gson.toJson(response)).build();
			}
		}catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create Mcq, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/mcqreport/{userid}/{subid}/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentMcqReport(@PathParam("userid") String userid,@PathParam("subid") String subid,@PathParam("cid") String cid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			McqReportDAO mcqReportDao=new McqReportDAO();
		
			response.setSuccess(true);
			response.setMessage("Mcq List");
			
			response.setData(mcqReportDao.getAllreportmcq(userid,subid,cid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
}
