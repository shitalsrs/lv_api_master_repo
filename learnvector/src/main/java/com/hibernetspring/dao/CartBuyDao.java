package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernetspring.model.Cartbuy;
import com.hibernetspring.model.Subject;

public class CartBuyDao {

	
	public Cartbuy saveCartbuy(Cartbuy cartbuy)
	{
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=sessionFactoryStock.openSession();
		    
			Session sessionStock=HibernateUtil.getSessionFactory().openSession();
			sessionStock.beginTransaction();
			sessionStock.save(cartbuy);
			sessionStock.getTransaction().commit();
			sessionStock.close();
			//sessionFactoryStock.close(); 
			
	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Session error"+e.getMessage());
			//sessionStock.getTransaction().rollback();
		}
		return cartbuy;
	}
	
	
	
	
	public List<Object> getCartbyuser(String userid)
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionMcq=HibernateUtil.getSessionFactory().openSession();
		
		try{
	
			
			String hql = "FROM Cartbuy cb where cb.u_name=:u_name and cb.incart=:incart";
			
			 int getid=1;
				Query query = sessionMcq.createQuery(hql);
				query.setParameter("u_name",userid);
				
				query.setParameter("incart",true);
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Object> veh=(ArrayList<Object>)results;	
			
		    
		    return veh;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionMcq.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
	
	public Boolean deleteCart(int cartid)
	{
	try{
   
		Session sessionStock=HibernateUtil.getSessionFactory().openSession();
		sessionStock.beginTransaction();
	
		
		String hql = "FROM Cartbuy cb where cb.cbid=:cbid";
			Query query = sessionStock.createQuery(hql);
			query.setParameter("cbid",cartid);
            List results = ((org.hibernate.Query) query).list();		
			
		    List<Cartbuy> veh=(ArrayList<Cartbuy>)results;	
			Cartbuy cartBuy=veh.get(0);
			    
			
		
		sessionStock.delete(cartBuy);
		sessionStock.getTransaction().commit();
		sessionStock.close();
		//sessionFactoryStock.close(); 
		 return true;

	}catch(Exception e){
		e.printStackTrace();
		System.out.println("Session error"+e.getMessage());
		//sessionStock.getTransaction().rollback();
	}
	return false;
	}
}
