package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="mcqlist_studentwise")
public class McqlistStudentwise {

	/*
	   `mcq_student_mapping_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `mcq_id` int(11) DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `date_of_expiry` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`mcq_student_mapping_id`)
	 */
	
	@Id
	@Column(name="mcq_student_mapping_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int mcq_student_mapping_id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="mcq_id")
	private int mcq_id;
	@Column(name="chapter_id")
	private int chapter_id;
	@Column(name="registration_id")
	private int registration_id;
	@Column(name="registration_id")
	private DateTime date_of_expiry;
		@Column(name="deleted")
		private TinyIntTypeDescriptor deleted;
		public int getMcq_student_mapping_id() {
			return mcq_student_mapping_id;
		}
		public void setMcq_student_mapping_id(int mcq_student_mapping_id) {
			this.mcq_student_mapping_id = mcq_student_mapping_id;
		}
		public int getStudent_id() {
			return student_id;
		}
		public void setStudent_id(int student_id) {
			this.student_id = student_id;
		}
		public int getMcq_id() {
			return mcq_id;
		}
		public void setMcq_id(int mcq_id) {
			this.mcq_id = mcq_id;
		}
		public int getChapter_id() {
			return chapter_id;
		}
		public void setChapter_id(int chapter_id) {
			this.chapter_id = chapter_id;
		}
		public int getRegistration_id() {
			return registration_id;
		}
		public void setRegistration_id(int registration_id) {
			this.registration_id = registration_id;
		}
		public DateTime getDate_of_expiry() {
			return date_of_expiry;
		}
		public void setDate_of_expiry(DateTime date_of_expiry) {
			this.date_of_expiry = date_of_expiry;
		}
		public TinyIntTypeDescriptor getDeleted() {
			return deleted;
		}
		public void setDeleted(TinyIntTypeDescriptor deleted) {
			this.deleted = deleted;
		}


}



