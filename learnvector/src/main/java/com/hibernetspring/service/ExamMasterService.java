package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.ExamMaster;
import com.hibernetspring.repository.ExamMasterRepo;

@Service
public class ExamMasterService {

	@Autowired
	private ExamMasterRepo examMasterRepo;
	
	public Response saveExamMaster(ExamMaster examMaster) {
		
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(examMasterRepo.save(examMaster));		
			response.setSuccess(true);
			response.setMessage("Exam master record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create exam master record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
