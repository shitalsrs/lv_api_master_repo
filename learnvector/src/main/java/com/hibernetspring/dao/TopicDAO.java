package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernetspring.model.Chapter;
import com.hibernetspring.model.Topic;

public class TopicDAO {

	public Topic saveTopic(Topic topic)
	{
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=sessionFactoryStock.openSession();
		    
			Session sessionStock=HibernateUtil.getSessionFactory().openSession();
			sessionStock.beginTransaction();
			sessionStock.save(topic);
			sessionStock.getTransaction().commit();
			sessionStock.close();
			//sessionFactoryStock.close();
			
	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Session error"+e.getMessage());
			//sessionStock.getTransaction().rollback();
		}
		return topic;
	}
	
	
	public List<Topic> getAllTopic(int chapterid,int subjectid)
	{
		List<Topic> topiclist=new ArrayList<Topic>();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
			 String hql = "FROM Topic t where t.chapterid=:chapterid and t.subjectid=:subjectid";
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
				query.setParameter("chapterid",chapterid);
				query.setParameter("subjectid",subjectid);
			List results = ((org.hibernate.Query) query).list();		
		    List<Topic> top=(ArrayList<Topic>)results;
		    
		    return top;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return topiclist;
	}
	
	
	public List<Topic> getAllTopicofchapter(int chapterid)
	{
		List<Topic> topiclist=new ArrayList<Topic>();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
			 String hql = "FROM Topic t where t.chapterid=:chapterid";
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
				query.setParameter("chapterid",chapterid);
				
			List results = ((org.hibernate.Query) query).list();		
		    List<Topic> top=(ArrayList<Topic>)results;
		    
		    return top;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return topiclist;
	}
	
}
