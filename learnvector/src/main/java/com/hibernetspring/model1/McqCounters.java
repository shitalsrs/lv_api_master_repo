package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="mcq_counters")
public class McqCounters {
/*
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `mcq_bank_id` int(11) DEFAULT NULL,
  `mcq_bank_id_last_updated` datetime DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="registration_id")
	private int registration_id;
	@Column(name="mcq_bank_id")
	private int mcq_bank_id;
	@Column(name="mcq_bank_id_last_updated")
	private DateTime mcq_bank_id_last_updated;
	@Column(name="chapter_id")
	private int chapter_id;
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	public int getRegistration_id() {
		return registration_id;
	}
	public void setRegistration_id(int registration_id) {
		this.registration_id = registration_id;
	}
	public int getMcq_bank_id() {
		return mcq_bank_id;
	}
	public void setMcq_bank_id(int mcq_bank_id) {
		this.mcq_bank_id = mcq_bank_id;
	}
	public DateTime getMcq_bank_id_last_updated() {
		return mcq_bank_id_last_updated;
	}
	public void setMcq_bank_id_last_updated(DateTime mcq_bank_id_last_updated) {
		this.mcq_bank_id_last_updated = mcq_bank_id_last_updated;
	}
	public int getChapter_id() {
		return chapter_id;
	}
	public void setChapter_id(int chapter_id) {
		this.chapter_id = chapter_id;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
}
