package com.hibernetspring.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model1.McqlistStudentwise;
import com.hibernetspring.repository.McqListStudentWiseRepo;

@Service
public class McqListStudentWiseService {
	@Autowired
	private McqListStudentWiseRepo mcqListStudentWiseRepo;
	
	public Response saveMcqListStudent(McqlistStudentwise mcqStudentWise) {
		
		
		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
		
			response.setData(mcqListStudentWiseRepo.save(mcqStudentWise));		
			response.setSuccess(true);
			response.setMessage("Mcq list studentwise record was created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create Mcq list studentwise record, Please try again after sometime!");
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
	}
}
