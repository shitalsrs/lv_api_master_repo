package com.hibernetspring.testapi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.dao.CoursesDAO;
import com.hibernetspring.dao.SubjectDao;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model.Courses;

@Path("course")
public class CoursesResource {

	@POST
	@Path("/addcourse")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createsubject(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Courses course = gson.fromJson(request,Courses.class);
			
			CoursesDAO coursesDao=new CoursesDAO();
			Boolean b=coursesDao.saveCourses(course);	
			if(b) {
			response.setSuccess(true);
			response.setMessage("course created created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();
		
			}else {
				response.setSuccess(false);
				response.setMessage("course not created successfully.");
				return Response.ok().status(200).entity(gson.toJson(response)).build();
			}
		}catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create course, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/subjectlist")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubjectDetailList(){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		CoursesDAO courseDao=new CoursesDAO();
		
			response.setSuccess(true);
			response.setMessage("Course List");
			
			response.setData(courseDao.getAllCourses());
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  course list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
    	
    	//return Response.ok().status(200).entity("I am good").build();
		
	}
	
	@GET
	@Path("/getsubjectdetail/{subjectid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubjectDetail(@PathParam("subjectid") int subjectid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		SubjectDao subjectDao=new SubjectDao();
		
			response.setSuccess(true);
			response.setMessage("subject deatil");
			
			response.setData(subjectDao.getsubjects(subjectid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
    	
    	//return Response.ok().status(200).entity("I am good").build();
		
	}
	
	
}
