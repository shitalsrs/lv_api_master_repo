package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hibernetspring.model1.PrevExamCounters;

import com.hibernetspring.service.PrevExamCountersService;
@RestController
@Path("preveexamcounter")
public class PrevexamCountersController {
	@Autowired
	private PrevExamCountersService prevExamCountersService;
	
	//add Preveexamcounter rest api
	
	@POST
	@Path("/addexamcounter")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMediaType(@RequestBody PrevExamCounters prevExamCounter) {
		
		return prevExamCountersService.savePrevExam(prevExamCounter);
		
	}
}
