package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="faq")
public class Faq {
	
	@Id
	@Column(name="faqid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int faqid;		
	@Column(name="qno")
	int qno;	
	@Column(name="question")
	String question;
	@Column(name="qdesc")
	String qdesc;
	@Column(name="subjectid")
	String subjectid;
	@Column(name="subjectname")	
	String subjectname;	
	@Column(name="chaptid")
	String chaptid;
	@Column(name="chaptname")
	String chaptname;
	
	
	public int getQno() {
		return qno;
	}
	public void setQno(int qno) {
		this.qno = qno;
	}
	public String getQdesc() {
		return qdesc;
	}
	public void setQdesc(String qdesc) {
		this.qdesc = qdesc;
	}
	public String getChaptid() {
		return chaptid;
	}
	public void setChaptid(String chaptid) {
		this.chaptid = chaptid;
	}
	public String getChaptname() {
		return chaptname;
	}
	public void setChaptname(String chaptname) {
		this.chaptname = chaptname;
	}
	public int getFaqid() {
		return faqid;
	}
	public void setFaqid(int faqid) {
		this.faqid = faqid;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(String subjectid) {
		this.subjectid = subjectid;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	
	
}
