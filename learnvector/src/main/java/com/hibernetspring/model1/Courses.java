package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="courses")
public class Courses {
/*
  `course_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_name` varchar(400) NOT NULL,
  `intro_video` int(11) DEFAULT NULL,
  `intro_text` int(11) DEFAULT NULL,
  `intro_image` int(11) DEFAULT NULL,
  `subject_group_code` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
 */
	
	@Id
	@Column(name="course_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int course_id;
	
	@Column(name="course_name")
	private String course_name;
	
	@Column(name="intro_video")
	private int intro_video;
	
	@Column(name="intro_text")
	private int intro_text;
	
	@Column(name="intro_image")
	private int intro_image;
	
	@Column(name="subject_group_code")
	private int subject_group_code;
	
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;

	
	
	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	public int getIntro_video() {
		return intro_video;
	}

	public void setIntro_video(int intro_video) {
		this.intro_video = intro_video;
	}

	public int getIntro_text() {
		return intro_text;
	}

	public void setIntro_text(int intro_text) {
		this.intro_text = intro_text;
	}

	public int getIntro_image() {
		return intro_image;
	}

	public void setIntro_image(int intro_image) {
		this.intro_image = intro_image;
	}

	public int getSubject_group_code() {
		return subject_group_code;
	}

	public void setSubject_group_code(int subject_group_code) {
		this.subject_group_code = subject_group_code;
	}

	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}

	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
	
	
}
