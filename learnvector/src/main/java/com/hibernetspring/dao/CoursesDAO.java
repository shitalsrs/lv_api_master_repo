package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.hibernetspring.model.Courses;


public class CoursesDAO {

	
	public boolean saveCourses(Courses courses)
	{boolean success = false;
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=sessionFactoryStock.openSession();
		    
			Session sessionCourses=HibernateUtil.getSessionFactory().openSession();
			sessionCourses.beginTransaction();
			sessionCourses.save(courses);
			sessionCourses.getTransaction().commit();
			sessionCourses.close();
			return true;
			
			//sessionFactoryStock.close();
			
	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Session error"+e.getMessage());
			//sessionStock.getTransaction().rollback();
		}
		return false;
	}
	
	
	public List<Object> getAllCourses()
	{
List<Object> courses=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionCourse=HibernateUtil.getSessionFactory().openSession();
		
		try{
	
			
			String hql = "FROM Courses s where s.courseid>=:courseid";
			
				
			 int getid=1;
				Query query = sessionCourse.createQuery(hql);
				
				query.setParameter("courseid",getid+"");
				
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Object> veh=(ArrayList<Object>)results;	
			
		    
		    return veh;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionCourse.close();
			//sessionFactoryVehical.close();
		}
		return courses;
	
	}
	public Courses getCourse(int cid )
	{
		Courses courses=new Courses();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
	
			
			String hql = "FROM Courses s where s.courseid=:courseid";
			
				
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
				query.setParameter("courseid",cid);
			
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Courses> veh=(ArrayList<Courses>)results;	
		    Courses cou=veh.get(0);
			    return cou;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return courses;
	}
}
