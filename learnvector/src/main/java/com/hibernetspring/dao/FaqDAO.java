package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernetspring.model.Faq;

public class FaqDAO {
	public FaqDAO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean savefaq(Faq faq)
	{
		Session sessionfaq=HibernateUtil.getSessionFactory().openSession();
		boolean success = false;
		try{
			
			
			sessionfaq.beginTransaction();
			sessionfaq.save(faq);
			sessionfaq.getTransaction().commit();			
	        success = true;
	         sessionfaq.close();
			
		
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("faq not created"+e.getMessage());
			
		      success = false;
        }

        return success;
		
	}
	
	
	public List<Object> getAllfaq(String chapterid)
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionfaq=HibernateUtil.getSessionFactory().openSession();
		
		try{
	
			
			String hql = "FROM Faq f where f.chaptid=:chaptid order by f.qno";
			
				
			 int getid=1;
				Query query = sessionfaq.createQuery(hql);
				query.setParameter("chaptid",chapterid);
			
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Object> faqlist=(ArrayList<Object>)results;	
			
		    
		    return faqlist;
			
			
		}catch(Exception e){
			
		}finally{
			sessionfaq.close();
			
		}
		return subjects;
	}

}
