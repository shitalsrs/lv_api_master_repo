package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.CourseChapMap;
import com.hibernetspring.service.CourseChapMapService;


@RestController
@Path("coursechapmap")
public class CourseChapMapController {
	@Autowired
	private CourseChapMapService CourseChapMapService;
	
	@POST
	@Path("/addcoursechapter")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCourseChapMap(@RequestBody CourseChapMap courseChapMap) {
		
		return CourseChapMapService.saveCourseMap(courseChapMap);
		
	
	}
}
