package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="MediaMaster")
public class MediaMaster {
	
	 /* `media_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `media_type_id` int(11) DEFAULT NULL,
	  `media_link` text DEFAULT NULL,
	  `last_update_datetime` datetime DEFAULT NULL,
	  `deleted` tinyint(1) DEFAULT NULL, */
	
	@Id
	@Column(name="media_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int media_id;
	@Column(name="media_type_id")
	private int media_type_id;
	@Column(name="media_link")
	private String media_link;
	@Column(name="last_update_datetime")
	private DateTime last_update_datetime;
	
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;

	public int getMedia_id() {
		return media_id;
	}

	public void setMedia_id(int media_id) {
		this.media_id = media_id;
	}

	public int getMedia_type_id() {
		return media_type_id;
	}

	public void setMedia_type_id(int media_type_id) {
		this.media_type_id = media_type_id;
	}

	public String getMedia_link() {
		return media_link;
	}

	public void setMedia_link(String media_link) {
		this.media_link = media_link;
	}

	public DateTime getLast_update_datetime() {
		return last_update_datetime;
	}

	public void setLast_update_datetime(DateTime last_update_datetime) {
		this.last_update_datetime = last_update_datetime;
	}

	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}

	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
}
