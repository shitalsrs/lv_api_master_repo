package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.StudentCoursePurchased;
import com.hibernetspring.service.StudCoursePurchasedService;



@RestController
@Path("studentcoursepurchased")
public class StudCoursePurchasedController {
	@Autowired
	private StudCoursePurchasedService studCoursePurchasedService;
	
	//add StudChaptPurchased rest api
	
	@POST
	@Path("/addchapterpurchased")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCoursePurchased(@RequestBody StudentCoursePurchased studCoursePurchased) {
		
		return studCoursePurchasedService.saveStudCoursePurchased(studCoursePurchased);
		
	}
}
