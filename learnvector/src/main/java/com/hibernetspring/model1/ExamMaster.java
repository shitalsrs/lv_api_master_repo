package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="exam_master")
public class ExamMaster {
/*
 
  `exam_master_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exam_name` varchar(200) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `exam_type` enum('bundled_with_course','separate_purchase') DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`exam_master_id`)
 */
	
	@Id
	@Column(name = "exam_master_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int exam_master_id;
	@Column(name = "exam_name")
	private String exam_name;

	@Column(name = "course_id")
	private int course_id;

	@Enumerated(EnumType.STRING)
	@Column(length = 40)
	private examType exam_type;

	public enum examType {
		bundled_with_course, separate_purchase
	}

	public int getExam_master_id() {
		return exam_master_id;
	}

	public void setExam_master_id(int exam_master_id) {
		this.exam_master_id = exam_master_id;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public examType getExam_type() {
		return exam_type;
	}

	public void setExam_type(examType exam_type) {
		this.exam_type = exam_type;
	}
	  
	 
}
