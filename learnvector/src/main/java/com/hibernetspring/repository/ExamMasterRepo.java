package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.ExamMaster;

public interface ExamMasterRepo extends JpaRepository<ExamMaster, Integer> {

}
