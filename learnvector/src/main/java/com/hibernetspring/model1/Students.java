package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="students")
public class Students {
/*
 `student_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL COMMENT 'comes from city master',
  `state` varchar(100) DEFAULT NULL COMMENT 'comes from state master',
  `country` varchar(100) DEFAULT NULL COMMENT 'comes from country master',
  `pin` int(11) DEFAULT NULL,
  `address_line_1` varchar(100) DEFAULT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `last_visited_from_server` varchar(100) DEFAULT NULL COMMENT 'e.g. 10.22.54.54',
  `student_registration_type` enum('visitor','registered','paid_registered') DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `last_login_analytics` text DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
 */
	@Id
	@Column(name = "student_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int student_id;
	@Column(name = "first_name")
	private String first_name;
	@Column(name = "middle_name")
	private String middle_name;
	@Column(name = "last_name")
	private String last_name;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "country")
	private String country;
	@Column(name = "pin")
	private int pin;
	@Column(name = "address_line_1")
	private String address_line_1;
	@Column(name = "address_line_2")
	private String address_line_2;
	@Column(name = "last_visited_from_server")
	private String last_visited_from_server;
	@Column(name = "student_registration_type")
	private String student_registration_type;
	@Column(name = "registration_id")
	private int registration_id;
	@Column(name = "registration_id")
	private String last_login_analytics;
	@Column(name = "deleted")
	private TinyIntTypeDescriptor deleted;
	  
	  
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	public String getAddress_line_1() {
		return address_line_1;
	}
	public void setAddress_line_1(String address_line_1) {
		this.address_line_1 = address_line_1;
	}
	public String getAddress_line_2() {
		return address_line_2;
	}
	public void setAddress_line_2(String address_line_2) {
		this.address_line_2 = address_line_2;
	}
	public String getLast_visited_from_server() {
		return last_visited_from_server;
	}
	public void setLast_visited_from_server(String last_visited_from_server) {
		this.last_visited_from_server = last_visited_from_server;
	}
	public String getStudent_registration_type() {
		return student_registration_type;
	}
	public void setStudent_registration_type(String student_registration_type) {
		this.student_registration_type = student_registration_type;
	}
	public int getRegistration_id() {
		return registration_id;
	}
	public void setRegistration_id(int registration_id) {
		this.registration_id = registration_id;
	}
	public String getLast_login_analytics() {
		return last_login_analytics;
	}
	public void setLast_login_analytics(String last_login_analytics) {
		this.last_login_analytics = last_login_analytics;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	  
	  
	  
	  
}
