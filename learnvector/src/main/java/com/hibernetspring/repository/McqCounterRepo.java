package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.McqCounters;

public interface McqCounterRepo extends JpaRepository<McqCounters, Integer> {

}
