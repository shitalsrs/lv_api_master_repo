package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hibernetspring.model1.StudentMcqPurchased;

import com.hibernetspring.service.StudentMcqPurchasedService;



@RestController
@Path("studentmcqpurchased")
public class StudentMcqPurchasedController {
	@Autowired
	private StudentMcqPurchasedService studentMcqPurchasedService;
	
	//add StudChaptPurchased rest api
	
	@POST
	@Path("/addstudentexam")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createStudentMcqPurchased(@RequestBody StudentMcqPurchased studentMcqPurchased) {
		
		return studentMcqPurchasedService.saveStudentMcqPurchased(studentMcqPurchased);
		
	}
	
}