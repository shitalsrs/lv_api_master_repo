package com.hibernetspring.model;
import javax.persistence.Access;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jgroups.annotations.Immutable;

//Add query in view update database lavel 
//select `trip`.`vehicaltrip`.`vt_id` AS `vt_id`,month(str_to_date(`trip`.`vehicaltrip`.`vt_entrydate`,'%d-%m-%Y')) AS `vt_month`,year(str_to_date(`trip`.`vehicaltrip`.`vt_entrydate`,'%d-%m-%Y')) AS `vt_year`,sum(`trip`.`vehicaltrip`.`vt_volume`) AS `vt_volume` from `trip`.`vehicaltrip` group by year(str_to_date(`trip`.`vehicaltrip`.`vt_entrydate`,'%d-%m-%Y')),month(str_to_date(`trip`.`vehicaltrip`.`vt_entrydate`,'%d-%m-%Y'))
//create view vehicaltripmonths as select `tripload`.`vehicaltrip`.`vt_id` AS `vt_id`,month(str_to_date(`tripload`.`vehicaltrip`.`vt_entrydate`,'%d-%m-%Y')) AS `vt_month`,year(str_to_date(`tripload`.`vehicaltrip`.`vt_entrydate`,'%d-%m-%Y')) AS `vt_year`,sum(`tripload`.`vehicaltrip`.`vt_volume`) AS `vt_volume` from `tripload`.`vehicaltrip` group by year(str_to_date(`tripload`.`vehicaltrip`.`vt_entrydate`,'%d-%m-%Y')),month(str_to_date(`tripload`.`vehicaltrip`.`vt_entrydate`,'%d-%m-%Y'))

@Entity
@Immutable
@Table(name="vehicaltripmonths")
public class Vehicaltripmonths {
	
	@Id
	@Column(name="vt_id")
	private int vt_id;
	
	@Column(name="vt_month")
    private String vt_month;
	
	@Column(name="vt_year")
    private String vt_year;
	
	@Column(name="vt_volume")
	   private Double vt_volume;

	public int getVt_id() {
		return vt_id;
	}

	public void setVt_id(int vt_id) {
		this.vt_id = vt_id;
	}

	public String getVt_month() {
		return vt_month;
	}

	public void setVt_month(String vt_month) {
		this.vt_month = vt_month;
	}

	public String getVt_year() {
		return vt_year;
	}

	public void setVt_year(String vt_year) {
		this.vt_year = vt_year;
	}

	public Double getVt_volume() {
		return vt_volume;
	}

	public void setVt_volume(Double vt_volume) {
		this.vt_volume = vt_volume;
	}
	
	
	
	
	
}
