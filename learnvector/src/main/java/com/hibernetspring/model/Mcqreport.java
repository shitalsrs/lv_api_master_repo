package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mcqreport")
public class Mcqreport {

	
	@Id
	@Column(name="mid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int mid;
	@Column(name="subjectid")
	private String subjectid;
	@Column(name="chapterid")
	private String chapterid;
	@Column(name="userid")
	private String userid;
	@Column(name="totalqno")
	private int totalqno;
	@Column(name="wanswer")
	private int wanswer;
	@Column(name="canswer")
	private int canswer;
	@Column(name="timetakeq")
	private String timetakeq;
	@Column(name="allocatedtime")
	private String allocatedtime;
	@Column(name="updatedatetime")
	private String updatedatetime;
	
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getChapterid() {
		return chapterid;
	}
	public void setChapterid(String chapterid) {
		this.chapterid = chapterid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public int getTotalqno() {
		return totalqno;
	}
	public void setTotalqno(int totalqno) {
		this.totalqno = totalqno;
	}
	public int getWanswer() {
		return wanswer;
	}
	public void setWanswer(int wanswer) {
		this.wanswer = wanswer;
	}
	public int getCanswer() {
		return canswer;
	}
	public void setCanswer(int canswer) {
		this.canswer = canswer;
	}
	public String getTimetakeq() {
		return timetakeq;
	}
	public void setTimetakeq(String timetakeq) {
		this.timetakeq = timetakeq;
	}
	public String getAllocatedtime() {
		return allocatedtime;
	}
	public void setAllocatedtime(String allocatedtime) {
		this.allocatedtime = allocatedtime;
	}
	public String getUpdatedatetime() {
		return updatedatetime;
	}
	public void setUpdatedatetime(String updatedatetime) {
		this.updatedatetime = updatedatetime;
	}

}
