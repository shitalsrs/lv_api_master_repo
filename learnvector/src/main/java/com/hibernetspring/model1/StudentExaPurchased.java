package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="student_exam_purchased")
public class StudentExaPurchased {
/*
 
 `exam_purchase_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'multiple entries per course',
  `student_id` int(11) DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `student_course_purchase_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `expired` tinyint(1) DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `exams_purchase_list_ids` text DEFAULT NULL COMMENT 'it is list of exam_master_id',
  PRIMARY KEY (`exam_purchase_id`)
 */
	@Id
	@Column(name="exam_purchase_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int exam_purchase_id;
	@Column(name="student_id")
	private int student_id;
	@Column(name="registration_id")
	private int registration_id;
	@Column(name="student_course_purchase_id")
	private int student_course_purchase_id;
	@Column(name="transaction_id")
	  private int transaction_id;
	  @Column(name="expired")
	  private TinyIntTypeDescriptor expired;
	 
	  @Column(name="expired_date")
	  private DateTime expired_date;
	  @Column(name="exams_purchase_list_ids")
	  private int exams_purchase_list_ids;
	  @Column(name="deleted")
		private TinyIntTypeDescriptor deleted;
	  
	public int getExam_purchase_id() {
		return exam_purchase_id;
	}
	public void setExam_purchase_id(int exam_purchase_id) {
		this.exam_purchase_id = exam_purchase_id;
	}
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	public int getRegistration_id() {
		return registration_id;
	}
	public void setRegistration_id(int registration_id) {
		this.registration_id = registration_id;
	}
	public int getStudent_course_purchase_id() {
		return student_course_purchase_id;
	}
	public void setStudent_course_purchase_id(int student_course_purchase_id) {
		this.student_course_purchase_id = student_course_purchase_id;
	}
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	public TinyIntTypeDescriptor getExpired() {
		return expired;
	}
	public void setExpired(TinyIntTypeDescriptor expired) {
		this.expired = expired;
	}
	public DateTime getExpired_date() {
		return expired_date;
	}
	public void setExpired_date(DateTime expired_date) {
		this.expired_date = expired_date;
	}
	public int getExams_purchase_list_ids() {
		return exams_purchase_list_ids;
	}
	public void setExams_purchase_list_ids(int exams_purchase_list_ids) {
		this.exams_purchase_list_ids = exams_purchase_list_ids;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	  
	  
	  
	  
}
