package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="course_chapmap")
public class CourseChapMap {

	/*
	   `course_chapter_mapping_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`course_chapter_mapping_id`)

	 */
	

	@Id
	@Column(name="course_chapter_mapping_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int course_chapter_mapping_id;

	@Column(name="course_name")
	private int course_id;

	@Column(name="course_name")
	private int chapter_id;
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	public int getCourse_chapter_mapping_id() {
		return course_chapter_mapping_id;
	}
	public void setCourse_chapter_mapping_id(int course_chapter_mapping_id) {
		this.course_chapter_mapping_id = course_chapter_mapping_id;
	}
	public int getCourse_id() {
		return course_id;
	}
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	public int getChapter_id() {
		return chapter_id;
	}
	public void setChapter_id(int chapter_id) {
		this.chapter_id = chapter_id;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
}
