package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernetspring.model.Topicreport;

public class TopicreportDAO {
	public Topicreport saveTopicreport(Topicreport topicreport)
	{
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=sessionFactoryStock.openSession();
		    
			Session sessionStock=HibernateUtil.getSessionFactory().openSession();
			sessionStock.beginTransaction();
			Object ob=sessionStock.save(topicreport);
			System.out.println("print object"+ob);
			sessionStock.getTransaction().commit();
			sessionStock.close();
			//sessionFactoryStock.close();
			
	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Session error"+e.getMessage());
			//sessionStock.getTransaction().rollback();
		}
		return topicreport;
	}
	
	
	public List<Topicreport> getAllreportTopic(String userid,int subid,int cid)
	{
		List<Topicreport> Topicreportlist=new ArrayList<Topicreport>();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
			 String hql = "FROM Topicreport t where t.usedrid=:usedrid and t.chapterid=:chapterid and t.subjectid=:subjectid";
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
				query.setParameter("usedrid",userid);
				query.setParameter("chapterid",cid);
				query.setParameter("subjectid",subid);
				
			List results = ((org.hibernate.Query) query).list();		
		    List<Topicreport> top=(ArrayList<Topicreport>)results;
		    
		    return top;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return Topicreportlist;
	}
}
