package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernetspring.model.Chapter;
import com.hibernetspring.model.ChapterTopic;
import com.hibernetspring.model.Stock;
import com.hibernetspring.model.Topic;

public class ChapterDAO {

	public Chapter saveChapter(Chapter chapter)
	{
		try{
			//sessionFactoryStock = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			//sessionStock=sessionFactoryStock.openSession();
		    
			Session sessionStock=HibernateUtil.getSessionFactory().openSession();
			sessionStock.beginTransaction();
			sessionStock.save(chapter);
			sessionStock.getTransaction().commit();
			sessionStock.close();
			//sessionFactoryStock.close();
			
	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Session error"+e.getMessage());
			//sessionStock.getTransaction().rollback();
		}
		return chapter;
	}
	
	
	public List<Object> getAllChapter(int subjectid)
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
			 String hql = "FROM  Chapter c where c.subid=:subid";
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
				query.setParameter("subid",subjectid);
			List results = ((org.hibernate.Query) query).list();		
		    List<Object> veh=(ArrayList<Object>)results;
		    
			/*
			 * String hqlsub = "FROM Subject s where s.subjectid>=:subjectid";
			 * 
			 * Query querys = sessionSubject.createQuery(hqlsub);
			 * query.setParameter("subjectid",subjectid); List rs = ((org.hibernate.Query)
			 * querys).list(); List<Object> vehs=(ArrayList<Object>)rs; veh.add(vehs);
			 */
		    
		    return veh;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}

	
	
	public List<Object> getAllChapterDetail(int subjectid)
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
			 String hql = "FROM  Chapter c where c.subid=:subid";
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
				query.setParameter("subid",subjectid);
			List results = ((org.hibernate.Query) query).list();		
		    List<Chapter> veh=(ArrayList<Chapter>)results;
		    
			
			  Iterator<Chapter> iterator = veh.iterator(); 
			  while (iterator.hasNext()) {
			  Chapter cp=new Chapter(); 
			  cp=iterator.next(); 
			  ChapterTopic chapt=new ChapterTopic();
			  chapt.setCid(cp.getCid()); 
			  chapt.setCname(cp.getCname()); 
			  int cids=cp.getCid(); 
			  TopicDAO topicdao=new TopicDAO();
			  chapt.setTopic(topicdao.getAllTopic(cids,subjectid));
			  subjects.add(chapt); 
			  System.out.println("Value is :"+ iterator.next()); }
			 
			/*
			 * for (Chapter s : veh) { ChapterTopic chapt=new ChapterTopic();
			 * chapt.setCid(s.getCid()); chapt.setCname(s.getCname()); int cids=s.getCid();
			 * TopicDAO topicdao=new TopicDAO();
			 * chapt.setTopic(topicdao.getAllTopic(s.getCid(),subjectid));
			 * 
			 * }
			 */
        
        
		    
			/*
			 * String hqlsub = "FROM Subject s where s.subjectid>=:subjectid";
			 * 
			 * Query querys = sessionSubject.createQuery(hqlsub);
			 * query.setParameter("subjectid",subjectid); List rs = ((org.hibernate.Query)
			 * querys).list(); List<Object> vehs=(ArrayList<Object>)rs; veh.add(vehs);
			 */
		    
		    return subjects;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
	
	public List<Object> getChapterDetail(int cid)
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionSubject=HibernateUtil.getSessionFactory().openSession();
		
		try{
			 String hql = "FROM  Chapter c where c.cid=:cid";
			 int getid=1;
				Query query = sessionSubject.createQuery(hql);
			
				query.setParameter("cid",cid);
			List results = ((org.hibernate.Query) query).list();		
		    List<Chapter> veh=(ArrayList<Chapter>)results;
		    
			
			  Iterator<Chapter> iterator = veh.iterator(); 
			  while (iterator.hasNext()) {
			  Chapter cp=new Chapter(); 
			  cp=iterator.next(); 
			  ChapterTopic chapt=new ChapterTopic();
			  chapt.setCid(cp.getCid()); 
			  chapt.setCname(cp.getCname()); 
			  int cids=cp.getCid(); 
			  TopicDAO topicdao=new TopicDAO();
			  chapt.setTopic(topicdao.getAllTopicofchapter(cids));
			  subjects.add(chapt); 
			  System.out.println("Value is :"+ iterator.next()); }
			 
		
		    
		    return subjects;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionSubject.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
	
	
	
	
}
