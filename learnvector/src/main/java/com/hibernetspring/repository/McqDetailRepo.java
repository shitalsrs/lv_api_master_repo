package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hibernetspring.model1.McqDetails;

public interface McqDetailRepo extends JpaRepository<McqDetails, Integer>{

}
