package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
import org.joda.time.DateTime;
@Entity
@Table(name ="login")
public class LoginCredentials {

	/*
	 
	   `login_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `credential_type` enum('gmail','linkedin','plain','facebook') DEFAULT NULL,
  `registration_id` varchar(100) DEFAULT NULL,
  `user_name` varchar(400) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL COMMENT 'hashed sha1 or something',
  `last_login_token` varchar(100) DEFAULT NULL COMMENT 'assigned for session management',
  `last_logout_time` datetime DEFAULT NULL,
  `last_logged_in_time` datetime DEFAULT NULL,
  `more_login_info` text DEFAULT NULL,
  `certificates` text DEFAULT NULL,
  `user_type` enum('student','admin','master_admin','teacher') DEFAULT NULL,
  `last_logged_in_server` varchar(100) DEFAULT NULL,
  `last_login_analytics` text DEFAULT NULL,
  `login_type` enum('live','standard') DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`login_id`)

	 */
	
	
	@Id
	@Column(name="login_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int login_id;
	
	@Enumerated(EnumType.STRING)
    @Column(length = 8)
    private credentialType credential_type;
	public enum credentialType {
		  gmail,linkedin,plain,facebook
		}
	@Column(name="registration_id")
	private String registration_id;
	@Column(name="user_name")
	private String user_name;
	@Column(name="password")
	private String password;
	
	@Column(name="last_login_token")
	private String last_login_token;
	
	@Column(name="last_logout_time")
	private DateTime last_logout_time;
	
	@Column(name="last_logged_in_time")
	private DateTime last_logged_in_time;
	
	@Column(columnDefinition="TEXT")
	private String more_login_info;
	
	@Column(columnDefinition="TEXT")
	private String certificates;
	
	
	@Enumerated(EnumType.STRING)
    @Column(length = 30)
    private userType user_type;
	  
	  
	  public enum userType {
		  student,admin,master_admin,teacher
		}
	
	  @Column(columnDefinition="TEXT")
		private String last_logged_in_server;
	  
	  @Column(columnDefinition="TEXT")
		private String last_login_analytics;
	
	  @Enumerated(EnumType.STRING)
	    @Column(length = 30)
		private loginType login_type;
	  
	  public enum loginType {
		  live,standard
		}
	  
	  
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;


	public int getLogin_id() {
		return login_id;
	}


	public void setLogin_id(int login_id) {
		this.login_id = login_id;
	}


	public credentialType getCredential_type() {
		return credential_type;
	}


	public void setCredential_type(credentialType credential_type) {
		this.credential_type = credential_type;
	}


	public String getRegistration_id() {
		return registration_id;
	}


	public void setRegistration_id(String registration_id) {
		this.registration_id = registration_id;
	}


	public String getUser_name() {
		return user_name;
	}


	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getLast_login_token() {
		return last_login_token;
	}


	public void setLast_login_token(String last_login_token) {
		this.last_login_token = last_login_token;
	}


	public DateTime getLast_logout_time() {
		return last_logout_time;
	}


	public void setLast_logout_time(DateTime last_logout_time) {
		this.last_logout_time = last_logout_time;
	}


	public DateTime getLast_logged_in_time() {
		return last_logged_in_time;
	}


	public void setLast_logged_in_time(DateTime last_logged_in_time) {
		this.last_logged_in_time = last_logged_in_time;
	}


	public String getMore_login_info() {
		return more_login_info;
	}


	public void setMore_login_info(String more_login_info) {
		this.more_login_info = more_login_info;
	}


	public String getCertificates() {
		return certificates;
	}


	public void setCertificates(String certificates) {
		this.certificates = certificates;
	}


	public userType getUser_type() {
		return user_type;
	}


	public void setUser_type(userType user_type) {
		this.user_type = user_type;
	}


	public String getLast_logged_in_server() {
		return last_logged_in_server;
	}


	public void setLast_logged_in_server(String last_logged_in_server) {
		this.last_logged_in_server = last_logged_in_server;
	}


	public String getLast_login_analytics() {
		return last_login_analytics;
	}


	public void setLast_login_analytics(String last_login_analytics) {
		this.last_login_analytics = last_login_analytics;
	}


	public loginType getLogin_type() {
		return login_type;
	}


	public void setLogin_type(loginType login_type) {
		this.login_type = login_type;
	}


	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}


	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}

	
	
	
}
