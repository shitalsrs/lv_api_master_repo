package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hibernetspring.model1.VersionInfo;

public interface VersionInfoRepo extends JpaRepository<VersionInfo, Integer>{
	
}
