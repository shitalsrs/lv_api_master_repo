package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class User {
	
	
	@Id
	@Column(name="u_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int u_id;
	
	@Column(unique = true)
	String u_name;
	
	@Column(name="u_fname")
	String u_fname;
	
	@Column(name="u_lname")
	String u_lname;
	
	
	@Column(name="u_password")
	String u_password;
	
	@Column(name="u_mobile",unique = true)
	String u_mobile;
	
	
	@Column(name="u_logindatetime")
	String u_logindatetime;
	
	@Column(name="u_createdatetime")
	String u_createdatetime;

	public int getU_id() {
		return u_id;
	}

	public void setU_id(int u_id) {
		this.u_id = u_id;
	}

	public String getU_name() {
		return u_name;
	}

	public void setU_name(String u_name) {
		this.u_name = u_name;
	}

	public String getU_password() {
		return u_password;
	}

	public void setU_password(String u_password) {
		this.u_password = u_password;
	}

	public String getU_logindatetime() {
		return u_logindatetime;
	}

	public void setU_logindatetime(String u_logindatetime) {
		this.u_logindatetime = u_logindatetime;
	}

	public String getU_createdatetime() {
		return u_createdatetime;
	}

	public void setU_createdatetime(String u_createdatetime) {
		this.u_createdatetime = u_createdatetime;
	}

	public String getU_fname() {
		return u_fname;
	}

	public void setU_fname(String u_fname) {
		this.u_fname = u_fname;
	}

	public String getU_lname() {
		return u_lname;
	}

	public void setU_lname(String u_lname) {
		this.u_lname = u_lname;
	}

	public String getU_mobile() {
		return u_mobile;
	}

	public void setU_mobile(String u_mobile) {
		this.u_mobile = u_mobile;
	}
	
	
	
	
	
}
