package com.hibernetspring.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernetspring.model.Mcq;
import com.hibernetspring.model.Subject;

public class McqDAO {
	
	
	public McqDAO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean saveMcq(Mcq mcq)
	{
		Session sessionMcq=HibernateUtil.getSessionFactory().openSession();
		boolean success = false;
		try{
			
			
			sessionMcq.beginTransaction();
			sessionMcq.save(mcq);
			sessionMcq.getTransaction().commit();			
	        success = true;
	         sessionMcq.close();
			
		
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("mcq not created"+e.getMessage());
			
		      success = false;
        }

        return success;
		
	}
	
	
	public List<Object> getAllmcq(String chapterid)
	{
		List<Object> subjects=new ArrayList<Object>();
		
	    Transaction tx = null;
	    Session sessionMcq=HibernateUtil.getSessionFactory().openSession();
		
		try{
	
			
			String hql = "FROM Mcq m where m.chapterid=:chapterid order by m.qno";
			
				
			 int getid=1;
				Query query = sessionMcq.createQuery(hql);
				query.setParameter("chapterid",chapterid);
			
			
			List results = ((org.hibernate.Query) query).list();		
			
		    List<Object> veh=(ArrayList<Object>)results;	
			
		    
		    return veh;
			
			//return users;
		}catch(Exception e){
			
		}finally{
			sessionMcq.close();
			//sessionFactoryVehical.close();
		}
		return subjects;
	}
}
