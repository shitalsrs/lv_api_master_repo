package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.Courses;
import com.hibernetspring.service.CoursesService;


@RestController
@Path("courses")
public class CoursesController {
	@Autowired
	private CoursesService coursesService;
	
	@POST
	@Path("/addcourse")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCourses(@RequestBody Courses courses) {
		
		return coursesService.saveCourse(courses);
		
	
	}
}
