package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.LoginCredentials;

public interface LoginCredentialRepo extends JpaRepository<LoginCredentials, Integer> {

}
