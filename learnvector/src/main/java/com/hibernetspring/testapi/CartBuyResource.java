package com.hibernetspring.testapi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.dao.CartBuyDao;
import com.hibernetspring.dao.ChapterDAO;
import com.hibernetspring.dao.McqReportDAO;
import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model.Cartbuy;
import com.hibernetspring.model.Chapter;

@Path("cart")
public class CartBuyResource {

	

	@POST
	@Path("/addcartbuy")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createStock(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Cartbuy cartbuy = gson.fromJson(request,Cartbuy.class);
			//System.out.println("gson data"+vehical.getV_carNo());
			
			CartBuyDao cartbuyDAO=new CartBuyDao();
			cartbuyDAO.saveCartbuy(cartbuy);		
			response.setSuccess(true);
			response.setMessage("Added into cart successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();

		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create cart, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/getsubjectlist/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentMcqReport(@PathParam("userid") String userid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			CartBuyDao cartBuyDao=new CartBuyDao();
		
			response.setSuccess(true);
			response.setMessage("getsubject List");
			
			response.setData(cartBuyDao.getCartbyuser(userid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
	
	

	@POST
	@Path("/deletcart/{cartid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteStock(@PathParam("cartid") int cartid) {
	
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
			CartBuyDao cartBuyDao=new CartBuyDao();
		
			response.setSuccess(true);
			response.setMessage("getsubject List");
			
			response.setData(cartBuyDao.deleteCart(cartid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
		
	}
	
}
