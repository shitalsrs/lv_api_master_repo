package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import com.hibernetspring.model1.MediaTypes;
import com.hibernetspring.service.MediaTypeService;

//@RestController
@Path("mediatype")
public class MediaTypesController {
	@Autowired
	private MediaTypeService mediaTypeService;
	
	@POST
	@Path("/addmediatype")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMediaType(@RequestBody MediaTypes mediaType) {
		return mediaTypeService.saveMediaType(mediaType);		
	}
}