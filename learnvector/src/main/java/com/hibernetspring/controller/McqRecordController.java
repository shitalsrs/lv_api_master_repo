package com.hibernetspring.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hibernetspring.model1.McqRecord;
import com.hibernetspring.service.McqRecordService;


@RestController
@Path("mcqrecord")
public class McqRecordController {
	
	@Autowired
	private McqRecordService mcqRecordService;
	
	@POST
	@Path("/addmcq")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMcqRecord(@RequestBody McqRecord mcqRecord) {
		
		return mcqRecordService.saveMcqRecord(mcqRecord);
		
	}
}
