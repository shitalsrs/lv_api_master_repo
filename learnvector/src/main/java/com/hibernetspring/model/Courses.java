package com.hibernetspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="courses")
public class Courses {

	
	@Id
	@Column(name="courseid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int courseid;
	@Column(name="coursename")
	String coursename;
	@Column(name="coursedesc")
	String coursedesc;
	@Column(name="chapterids")
	int chapterids;
	
	@Column(name="subjectnames")
	String subjectnames;
	
	@Column(name="subjectids")
	String subjectids;
	
	@Column(name="subjecttype")
	String subjecttype;
	
	@Column(name="price")
	String price;
	
	
	@Column(name="rating")
	int rating;
	
	@Column(name="teacher")
	String teacher;
	
	
	
	@Lob
	String description;
	
	@Column(name="imgur")
	String imgur;
	@Column(name="videourl")
	String videourl;
	
	@Column(name="updatedatetime")
	String updatedatetime;
	
	public int getCourseid() {
		return courseid;
	}
	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}
	public String getCoursename() {
		return coursename;
	}
	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
	public String getCoursedesc() {
		return coursedesc;
	}
	public void setCoursedesc(String coursedesc) {
		this.coursedesc = coursedesc;
	}
	public int getChapterids() {
		return chapterids;
	}
	public void setChapterids(int chapterids) {
		this.chapterids = chapterids;
	}
	public String getSubjecttype() {
		return subjecttype;
	}
	public void setSubjecttype(String subjecttype) {
		this.subjecttype = subjecttype;
	}
	public String getSubjectnames() {
		return subjectnames;
	}
	public void setSubjectnames(String subjectnames) {
		this.subjectnames = subjectnames;
	}
	public String getSubjectids() {
		return subjectids;
	}
	public void setSubjectids(String subjectids) {
		this.subjectids = subjectids;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
	
	
	

}
