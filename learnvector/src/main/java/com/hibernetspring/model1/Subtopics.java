package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="subtopics")
public class Subtopics {

	/*
	   `subtopic_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chapter_id` int(11) NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subtopic_name` varchar(500) NOT NULL,
  `subtopic_image` int(11) DEFAULT NULL COMMENT 'come from media_table',
  `subtopic_intro_video` int(11) DEFAULT NULL COMMENT 'come from media_table',
  `subtopic_intro_text` int(11) DEFAULT NULL COMMENT 'come from media_table',
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`subtopic_id`)

	 */
	@Id
	@Column(name="subtopic_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int subtopic_id;
	@Column(name="chapter_id")
	private int chapter_id;
	@Column(name="subject_id")
	private int subject_id;
	@Column(name="subtopic_name")
	private String subtopic_name;
	@Column(name="subtopic_image")
	private int subtopic_image;
	@Column(name="subtopic_intro_video")
	private int subtopic_intro_video;
	@Column(name="subtopic_intro_text")
	private int subtopic_intro_text;
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	public int getSubtopic_id() {
		return subtopic_id;
	}
	public void setSubtopic_id(int subtopic_id) {
		this.subtopic_id = subtopic_id;
	}
	public int getChapter_id() {
		return chapter_id;
	}
	public void setChapter_id(int chapter_id) {
		this.chapter_id = chapter_id;
	}
	public int getSubject_id() {
		return subject_id;
	}
	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}
	public String getSubtopic_name() {
		return subtopic_name;
	}
	public void setSubtopic_name(String subtopic_name) {
		this.subtopic_name = subtopic_name;
	}
	public int getSubtopic_image() {
		return subtopic_image;
	}
	public void setSubtopic_image(int subtopic_image) {
		this.subtopic_image = subtopic_image;
	}
	public int getSubtopic_intro_video() {
		return subtopic_intro_video;
	}
	public void setSubtopic_intro_video(int subtopic_intro_video) {
		this.subtopic_intro_video = subtopic_intro_video;
	}
	public int getSubtopic_intro_text() {
		return subtopic_intro_text;
	}
	public void setSubtopic_intro_text(int subtopic_intro_text) {
		this.subtopic_intro_text = subtopic_intro_text;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
}
