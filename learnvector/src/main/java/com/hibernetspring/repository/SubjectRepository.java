package com.hibernetspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hibernetspring.model1.Subjects;

public interface SubjectRepository extends JpaRepository<Subjects,Integer> {

}
