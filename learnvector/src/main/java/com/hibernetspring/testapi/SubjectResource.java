package com.hibernetspring.testapi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hibernetspring.dao.SubjectDao;

import com.hibernetspring.misc.WebApiResponse;
import com.hibernetspring.model.Subject;

@RestController
@Path("subjects")
public class SubjectResource {

	@POST
	@Path("/addsubject")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createsubject(String request) {

		WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {
			Subject subject = gson.fromJson(request,Subject.class);
			
			SubjectDao subjecttDao=new SubjectDao();
			Boolean b=subjecttDao.saveSubject(subject);	
			if(b) {
			response.setSuccess(true);
			response.setMessage("Subject created created successfully.");
			return Response.ok().status(200).entity(gson.toJson(response)).build();
				/*
				 * return Response .status(200) .header("Access-Control-Allow-Origin", "*")
				 * .header("Access-Control-Allow-Credentials", "true")
				 * .header("Access-Control-Allow-Headers",
				 * "origin, content-type, accept, authorization")
				 * .header("Access-Control-Allow-Methods",
				 * "GET, POST, PUT, DELETE, OPTIONS, HEAD") .entity(gson.toJson(response))
				 * .build();
				 */
			}else {
				response.setSuccess(false);
				response.setMessage("Subject not created successfully.");
				return Response.ok().status(200).entity(gson.toJson(response)).build();
			}
		}catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to create subject, Please try again after sometime!");
			//logger.error("Unable to create user, Please try again after sometime!"+ ex);
			return Response.serverError().status(500)
					.entity(gson.toJson(response)).build();
		}
		
	}
	
	
	@GET
	@Path("/subjectlist")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubjectDetailList(){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		SubjectDao subjectDao=new SubjectDao();
		
			response.setSuccess(true);
			response.setMessage("subject List");
			
			response.setData(subjectDao.getAllsubjects());
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
    	
    	//return Response.ok().status(200).entity("I am good").build();
		
	}
	
	@GET
	@Path("/getsubjectdetail/{subjectid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubjectDetail(@PathParam("subjectid") int subjectid){
    	WebApiResponse response = new WebApiResponse();
		Gson gson = new GsonBuilder().create();
		
		try {		
		SubjectDao subjectDao=new SubjectDao();
		
			response.setSuccess(true);
			response.setMessage("subject deatil");
			
			response.setData(subjectDao.getsubjects(subjectid));
			return Response.ok().status(200).entity(gson.toJson(response)).build();
			
		} catch (Exception ex) {
			response.setSuccess(false);
			response.setMessage("Unable to get  subject list, Please try again after sometime!");
			return Response.serverError().status(500).entity(gson.toJson(response)).build();
		}
    	
    	//return Response.ok().status(200).entity("I am good").build();
		
	}
	
	
}
