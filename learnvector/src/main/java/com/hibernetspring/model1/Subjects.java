package com.hibernetspring.model1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;
@Entity
@Table(name ="subjects")
public class Subjects {

	/*
	  `subject_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(200) DEFAULT NULL,
  `intro_text` int(11) DEFAULT NULL,
  `intro_image` int(11) DEFAULT NULL,
  `intro_video` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`subject_id`)
	 */
	
	@Id
	@Column(name="subject_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int subject_id;
	@Column(name="subject_name")
	private String subject_name;
	@Column(name="intro_text")
	private int intro_text;
	@Column(name="intro_image")
	private int intro_image;
	@Column(name="intro_video")
	private int intro_video;
	@Column(name="deleted")
	private TinyIntTypeDescriptor deleted;
	public int getSubject_id() {
		return subject_id;
	}
	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}
	public String getSubject_name() {
		return subject_name;
	}
	public void setSubject_name(String subject_name) {
		this.subject_name = subject_name;
	}
	public int getIntro_text() {
		return intro_text;
	}
	public void setIntro_text(int intro_text) {
		this.intro_text = intro_text;
	}
	public int getIntro_image() {
		return intro_image;
	}
	public void setIntro_image(int intro_image) {
		this.intro_image = intro_image;
	}
	public int getIntro_video() {
		return intro_video;
	}
	public void setIntro_video(int intro_video) {
		this.intro_video = intro_video;
	}
	public TinyIntTypeDescriptor getDeleted() {
		return deleted;
	}
	public void setDeleted(TinyIntTypeDescriptor deleted) {
		this.deleted = deleted;
	}
	
	
	
	
}
